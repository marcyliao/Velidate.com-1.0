<?php
require_once __DIR__ . '/../../../utility/Properties.class.php';
/*
 * Connection properties
 *
 * @author: http://phpdao.com
 * @date: 27.11.2007
 */
class ConnectionProperty{

	public static function getHost(){
		return Properties::$RDS_HOST;
	}

	public static function getUser(){
		return Properties::$RDS_USERNAME;
	}

	public static function getPassword(){
		return Properties::$RDS_PASSWORD;
	}

	public static function getDatabase(){
		return Properties::$RDS_DATABASE;
	}
}
?>