<?php

require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__.'/../../dto/Feedback.class.php';

/*
 * Class that operate on table 'feedback'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-08-19
 */
class FeedbackMySqlDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return feedbackMySql 
	 */
	public function load($id){
		$sql = 'SELECT * FROM feedback WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM feedback';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM feedback ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param feedback primary key
 	 */
	public function delete($id){
		$sql = 'DELETE FROM feedback WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($id);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param feedbackMySql feedback
 	 */
	public function insert($feedback){
		$sql = 'INSERT INTO feedback (createTime, content, userId) VALUES (?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($feedback->createTime);
		$sqlQuery->set($feedback->content);
		$sqlQuery->setNumber($feedback->userId);

		$id = $this->executeInsert($sqlQuery);	
		$feedback->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param feedbackMySql feedback
 	 */
	public function update($feedback){
		$sql = 'UPDATE feedback SET createTime = ?, content = ?, userId = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($feedback->createTime);
		$sqlQuery->set($feedback->content);
		$sqlQuery->setNumber($feedback->userId);

		$sqlQuery->setNumber($feedback->id);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'TRUNCATE TABLE feedback';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
	 * Read row
	 *
	 * @return FeedbackMySql 
	 */
	protected function readRow($row){
		$feedback = new Feedback();
		
		$feedback->id = $row['id'];
		$feedback->createTime = $row['createTime'];
		$feedback->content = $row['content'];
		$feedback->userId = $row['userId'];

		return $feedback;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return feedbackMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab) == 0) {
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>