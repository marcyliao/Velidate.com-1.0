<?php
require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__ . '/../../dto/Photo.class.php';

/*
 * Class that operate on table 'photo'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class PhotoMySqlDAO {

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return PhotoMySql
	 */
	public function load($id) {
		$sql = 'SELECT * FROM photo WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($id);
		return $this -> getRow($sqlQuery);
	}

	public function loadByContactId($cid) {
		$sql = 'SELECT * FROM photo WHERE contactId = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($cid);
		return $this -> getList($sqlQuery);
	}

	public function loadMainPhotoByContactId($cid) {
		$sql = 'SELECT * FROM photo WHERE contactId = ? and isMainPhoto = 1';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($cid);
		return $this -> getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll() {
		$sql = 'SELECT * FROM photo';
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn) {
		$sql = 'SELECT * FROM photo ORDER BY ' . $orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this -> getList($sqlQuery);
	}

	/**
	 * Delete record from table
	 * @param photo primary key
	 */
	public function delete($id) {
		$sql = 'DELETE FROM photo WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery -> setNumber($id);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Insert record to table
	 *
	 * @param PhotoMySql photo
	 */
	public function insert($photo) {
		$sql = 'INSERT INTO photo (contactId, verified, notVerified, linkOriginal, linkLarge, linkMedium, linkSmall, visibility, isMainPhoto, createTime, updateTime) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);

		$sqlQuery -> setNumber($photo -> contactId);
		$sqlQuery -> setNumber($photo -> verified);
		$sqlQuery -> setNumber($photo -> notVerified);
		$sqlQuery -> set($photo -> linkOriginal);
		$sqlQuery -> set($photo -> linkLarge);
		$sqlQuery -> set($photo -> linkMedium);
		$sqlQuery -> set($photo -> linkSmall);
		$sqlQuery -> setNumber($photo -> visibility);
		$sqlQuery -> setNumber($photo -> isMainPhoto);
		$sqlQuery -> set($photo -> createTime);
		$sqlQuery -> set($photo -> updateTime);

		$id = $this -> executeInsert($sqlQuery);
		$photo -> id = $id;
		return $id;
	}

	/**
	 * Update record in table
	 *
	 * @param PhotoMySql photo
	 */
	public function update($photo) {
		$sql = 'UPDATE photo SET contactId = ?, verified = ?, notVerified = ?, linkOriginal = ?, linkLarge = ?, linkMedium = ?, linkSmall = ?, visibility = ?, isMainPhoto = ?, createTime = ?, updateTime = ? WHERE id = ?';
		$sqlQuery = new SqlQuery($sql);

		$sqlQuery -> setNumber($photo -> contactId);
		$sqlQuery -> setNumber($photo -> verified);
		$sqlQuery -> setNumber($photo -> notVerified);
		$sqlQuery -> set($photo -> linkOriginal);
		$sqlQuery -> set($photo -> linkLarge);
		$sqlQuery -> set($photo -> linkMedium);
		$sqlQuery -> set($photo -> linkSmall);
		$sqlQuery -> setNumber($photo -> visibility);
		$sqlQuery -> setNumber($photo -> isMainPhoto);
		$sqlQuery -> set($photo -> createTime);
		$sqlQuery -> set($photo -> updateTime);

		$sqlQuery -> setNumber($photo -> id);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Delete all rows
	 */
	public function clean() {
		$sql = 'TRUNCATE TABLE photo';
		$sqlQuery = new SqlQuery($sql);
		return $this -> executeUpdate($sqlQuery);
	}

	/**
	 * Read row
	 *
	 * @return PhotoMySql
	 */
	protected function readRow($row) {
		$photo = new Photo();

		$photo -> id = $row['id'];
		$photo -> contactId = $row['contactId'];
		$photo -> verified = $row['verified'];
		$photo -> notVerified = $row['notVerified'];
		$photo -> linkOriginal = $row['linkOriginal'];
		$photo -> linkLarge = $row['linkLarge'];
		$photo -> linkMedium = $row['linkMedium'];
		$photo -> linkSmall = $row['linkSmall'];
		$photo -> visibility = $row['visibility'];
		$photo -> isMainPhoto = $row['isMainPhoto'];
		$photo -> createTime = $row['createTime'];
		$photo -> updateTime = $row['updateTime'];

		return $photo;
	}

	protected function getList($sqlQuery) {
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for ($i = 0; $i < count($tab); $i++) {
			$ret[$i] = $this -> readRow($tab[$i]);
		}
		return $ret;
	}

	/**
	 * Get row
	 *
	 * @return PhotoMySql
	 */
	protected function getRow($sqlQuery) {
		$tab = QueryExecutor::execute($sqlQuery);
		if (count($tab) == 0) {
			return null;
		}
		return $this -> readRow($tab[0]);
	}

	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery) {
		return QueryExecutor::execute($sqlQuery);
	}

	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery) {
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery) {
		return QueryExecutor::executeInsert($sqlQuery);
	}

}
?>