<?php

require_once 'sql/SqlQuery.class.php';
require_once 'sql/QueryExecutor.class.php';
require_once 'sql/Transaction.class.php';
require_once 'sql/Connection.class.php';
require_once 'sql/ConnectionFactory.class.php';
require_once 'sql/ConnectionProperty.class.php';
require_once __DIR__.'/../../dto/Institute.class.php';

/*
 * Class that operate on table 'institute'. Database Mysql.
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class InstituteMySqlDAO{

	/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return InstituteMySql 
	 */
	public function load($collegeDomain){
		$sql = 'SELECT * FROM institute WHERE domainName = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($collegeDomain);
		return $this->getRow($sqlQuery);
	}

/**
	 * Get Domain object by primry key
	 *
	 * @param String $id primary key
	 * @return InstituteMySql 
	 */
	public function loadLike($college){
		$college = $college . "%";
		$sql = 'SELECT * FROM institute WHERE domainName like ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($college);
		return $this->getRow($sqlQuery);
	}
	
	/**
	 * Get Domain object from an email
	 *
	 * @param String $id primary key
	 * @return InstituteMySql 
	 */
	public function findInstitute($email){
		$sql = 'SELECT * FROM institute WHERE ? like concat("%", domainName, "%")';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->set($email);
		return $this->getRow($sqlQuery);
	}

	/**
	 * Get all records from table
	 */
	public function queryAll(){
		$sql = 'SELECT * FROM institute';
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
	 * Get all records from table ordered by field
	 *
	 * @param $orderColumn column name
	 */
	public function queryAllOrderBy($orderColumn){
		$sql = 'SELECT * FROM institute ORDER BY '.$orderColumn;
		$sqlQuery = new SqlQuery($sql);
		return $this->getList($sqlQuery);
	}
	
	/**
 	 * Delete record from table
 	 * @param institute primary key
 	 */
	public function delete($domainName){
		$sql = 'DELETE FROM institute WHERE domainName = ?';
		$sqlQuery = new SqlQuery($sql);
		$sqlQuery->setNumber($domainName);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
 	 * Insert record to table
 	 *
 	 * @param InstituteMySql institute
 	 */
	public function insert($institute){
		$sql = 'INSERT INTO institute (instituteName, address) VALUES (?, ?, ?)';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($institute->instituteName);
		$sqlQuery->setNumber($institute->cityId);
		$sqlQuery->set($institute->address);

		$id = $this->executeInsert($sqlQuery);	
		$institute->id = $id;
		return $id;
	}
	
	/**
 	 * Update record in table
 	 *
 	 * @param InstituteMySql institute
 	 */
	public function update($institute){
		$sql = 'UPDATE institute SET instituteName = ?, address = ? WHERE domainName = ?';
		$sqlQuery = new SqlQuery($sql);
		
		$sqlQuery->set($institute->instituteName);
		$sqlQuery->setNumber($institute->cityId);
		$sqlQuery->set($institute->address);

		$sqlQuery->setNumber($institute->domainName);
		return $this->executeUpdate($sqlQuery);
	}

	/**
 	 * Delete all rows
 	 */
	public function clean(){
		$sql = 'TRUNCATE TABLE institute';
		$sqlQuery = new SqlQuery($sql);
		return $this->executeUpdate($sqlQuery);
	}
	
	/**
	 * Read row
	 *
	 * @return InstituteMySql 
	 */
	protected function readRow($row){
		$institute = new Institute();
		
		$institute->domainName = $row['domainName'];
		$institute->instituteName = $row['instituteName'];
		$institute->address = $row['address'];

		return $institute;
	}
	
	protected function getList($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		$ret = array();
		for($i=0;$i<count($tab);$i++){
			$ret[$i] = $this->readRow($tab[$i]);
		}
		return $ret;
	}
	
	/**
	 * Get row
	 *
	 * @return InstituteMySql 
	 */
	protected function getRow($sqlQuery){
		$tab = QueryExecutor::execute($sqlQuery);
		if(count($tab) == 0) {
			return null;
		}
		return $this->readRow($tab[0]);		
	}
	/**
	 * Execute sql query
	 */
	protected function execute($sqlQuery){
		return QueryExecutor::execute($sqlQuery);
	}
	
		
	/**
	 * Execute sql query
	 */
	protected function executeUpdate($sqlQuery){
		return QueryExecutor::executeUpdate($sqlQuery);
	}

	/**
	 * Insert row to table
	 */
	protected function executeInsert($sqlQuery){
		return QueryExecutor::executeInsert($sqlQuery);
	}
}
?>