<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
	echo "Starting testPhoto";
	//echo "File: " . __FILE__;
	//echo "  Dir: " . __DIR__;
	//echo "  Root: " . __ROOT__;
	require_once __DIR__.'/../service/PictureService.class.php';
	require_once __DIR__ . "/../utility/Properties.class.php";
	echo "after require <br>";
	$storePIC = new PictureService();
	$thispic = new Photo;
	//EmailService::$header = "";
	
	// Send email if SubOrder is set
	
	if (isset($_POST['SubOrder'])) {
		echo "before processing pic <br>";
		$cid = $_POST['cid'];
		//$filename = $_POST['pict'];
		
		$name = $_FILES["picture"]["name"];
		$ext = end(explode(".", $name));
		//$filepath = "../photos/$cid" . "_tmp" . "." . $ext;
		$filepath = Properties::$ROOT_PATH . "photos/$cid" . "_tmp" . "." . $ext;

		move_uploaded_file ($_FILES['picture'] ['tmp_name'], $filepath);
		chmod($filepath, 0666);
		
		$thispic->contactId = $cid;
		$thispic->verified = 0;
		$thispic->notVerified = 0;
		$thispic->linkOriginal = "No link";
		$thispic->linkLarge = "No link";
		$thispic->linkMedium = "No link";
		$thispic->linkSmall = "No link";
		$thispic->visibility = 0;
		$thispic->isMainPhoto = 0;
		
		// $thispic is an object of Photo Class.
		// $filepath contains the path and filename of the image
		// Convert photo to jpg, and store them in original, medium and small
		// Store in directory p0 for ID 1 to 999; p1 for ID 1000 to 1999 and so on.
		// File name is <ID>_o for original; _m for medium; _s for small.
		$storePIC->putpic($thispic, $filepath);
		
		echo "after processing pic <br>";
	}
?>
	
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<p>Testing Photo Services</p>
<form id="order_detail" action="testPhoto.php" method="post" enctype='multipart/form-data'>
  <p>Contact ID: 
    <input size="30" maxlength="30" name="cid"><br />
    Picture Path: 
    <input type="hidden" name="MAX_FILE_SIZE" value="8000000" />
    <input size="50" name='picture' type='file' accept="image/*" />
    <input value="Submit: Store Pic" type="submit" name="SubOrder">
  </p>
</form>


</body>
</html>