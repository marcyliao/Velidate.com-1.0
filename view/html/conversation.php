<?php
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/PictureService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
	require_once __DIR__."/../../service/MessageService.class.php";

    try {

	    require "component/userAuth.php";
	    require "component/userProfilePic.php";

		$now = new DateTime();
		$birth = new DateTime($me->birthday);
		$age = $now->format('Y') - $birth->format('Y');

		$messageService = new MessageService();
		$allConvers = $messageService->getConversationList($mid,round(microtime(true) * 1000),50);
		if($allConvers==null || count($allConvers)==0) {
			$msg = "You don't have any messages yet. Please start sending messages on profile pages of other users.";

        	Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg,false);
		}
	}
	catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
        <link rel="stylesheet" href="../libs/jquery-scrollbar/jquery.mCustomScrollbar.css" />
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div id="message-box" class="alert alert-info" style="display:none"></div>
        <div id="error-box" class="alert alert-danger" style="display:none"></div>
        <div class="site-wrapper for-footer-header">
            <div class="site-wrapper-inner">
                <?php require "component/navigation.php"; ?>
                <div class="main-content">
                    <div class="content-container">
                        <h2 style="margin-bottom: 20px">Messages</h2>
                        <div class="column-conversation">
                        	<div id="conversations-loading" class="list-loading">Loading..</div>
                        	<div class="conversations-list-wrapper">
	                        	<div class="conversations-list">
	                            
	                            </div>
                            </div>
                        </div>
                        <div class="column-messages">
                        	<div id="messages-loading" class="list-loading">Loading..</div>
                        	<div class="messages-list-wrapper">
	                        	<div class="messages-list">
	                        		
	                            </div>
                           </div>
                            <div class="message-input-group">
                            	<textarea id="input-message" class="form-control" placeholder="Message"></textarea>
                            	<div class="send-button-wrapper">
                            		<a id="send-btn" class="button button_ss blue" href="#">Send</a>
                            	</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
	<script src="../libs/jquery-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="//js.pusher.com/2.2/pusher.min.js"></script>
	<script>
		String.prototype.string = function(l) { var s = '', i = 0; while (i++ < l) { s += this; } return s; }
		String.prototype.zf = function(l) { return '0'.string(l - this.length) + this; }
		Number.prototype.zf = function(l) { return this.toString().zf(l); }
		
		// a global month names array
		var gsMonthNames = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
		// a global day names array
		var gsDayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		// the date format prototype
		Date.prototype.format = function(f) {
			if (!this.valueOf())
				return '&nbsp;';

			var d = this;

			return f.replace(/(yyyy|mmmm|mmm|mm|dddd|ddd|dd|hh|nn|ss|a\/p)/gi, function($1) {
				switch ($1.toLowerCase()) {
					case 'yyyy':
						return d.getFullYear();
					case 'mmmm':
						return gsMonthNames[d.getMonth()];
					case 'mmm':
						return gsMonthNames[d.getMonth()].substr(0, 3);
					case 'mm':
						return (d.getMonth() + 1).zf(2);
					case 'dddd':
						return gsDayNames[d.getDay()];
					case 'ddd':
						return gsDayNames[d.getDay()].substr(0, 3);
					case 'dd':
						return d.getDate().zf(2);
					case 'hh':
						return d.getHours().zf(2);
					case 'nn':
						return d.getMinutes().zf(2);
					case 'ss':
						return d.getSeconds().zf(2);
					case 'a/p':
						return d.getHours() < 12 ? 'a' : 'p';
				}
			});
		}
	</script>
	<script>
		var currentConversatoinId = null;
		var currentTargetId = null;
		var myId = <?php echo $mid ?>;
		var myName = "<?php echo $me->userName ?>";
		var myPhoto = "<?php echo $mMainPicLink ?>";
		
		function readConversation(cid) {
			$.ajax({
			  url: "<?php echo Properties::$MESSAGE_READ_CONVERSATION_ACTION ?>",
			  data: {conversationId: cid},
			});
			
			$("#unread-msg-num-"+cid).text("0");
			$("#unread-msg-num-"+cid).hide();
		}
		
		function readMessage(mid) {
			$.ajax({
			  url: "<?php echo Properties::$MESSAGE_READ_MESSAGE_ACTION ?>",
			  data: {messageId: mid},
			});
		}
		
		function sendMessage(content,callback) {
			
			$.ajax({
                type: "GET",
                data: {toId:currentTargetId, content: content},
                url: "<?php echo Properties::$MESSAGE_SEND_MESSAGE_ACTION ?>",
                success: function(data) {
	           		var html = '</div><div class="message-wrapper">'+
                        			'<div class="message-target-image"><img src="'+myPhoto+'"></div>'+
                        			'<div class="messgae-detail">'+
                        				'<div class="message-target-name"><a href="<?php echo Properties::$PROFILE_PAGE ?>?id='+myId+'">'+myName+'</a></div>'+
                        				'<div class="message-body">'+
                        					'<div class="message-text">'+
                        						content+
                        					'</div>'+
                        					'<div class="message-time">'+
                        						new Date().format('hh:nn mmmm dd')+
                        					'</div>'+
                        				'</div>'+
                        			'</div>'+
                        		'</div>';
                	$(".messages-list").append(html);
                
					callback();
					setTimeout(function(){
						$(".messages-list-wrapper").mCustomScrollbar("scrollTo","bottom")
					},100);
                }
            });
		}
		
		function receiveNewMessage(msg) {
			
			if($(".conversation-group[data-id='"+msg.conversationId+"']").length == 0) { // the conversation is not in the list
				
				var imgUrl = null;
				if(msg.targetPhoto==null)
					imgUrl = "../imgs/profile-default.png"
				else
				    imgUrl = msg.targetPhoto.linkSmall;
				
				var html = '<div data-id="'+msg.conversationId+'" class="conversation-group"  data-target-id="'+msg.targetUserId+'" >'+
                            		'<div class="conversation-background-wrapper"  data-id="'+msg.conversationId+'" data-target-id="'+msg.targetUserId+'" >'+
                            		'<div class="conversation-pic">'+
                                        '<div class="align-marker"></div><img id="conversation-img-'+msg.conversationId+'" src="'+imgUrl+'">'+
                                    '</div>'+
                                    '<div id="conversation-name-'+msg.conversationId+'"  data-target-id="'+msg.targetUserId+'" class="conversation-name"><div>'+
                                    	msg.targetName + 
                                    '</div></div>'+
					 				'<div id="unread-msg-num-'+msg.conversationId+'" class="unread-msg-num" style="display:none">1</div>' +
                                    '</div>'+
                            	'</div>';
                            	
                $(".conversations-list").prepend(html);
                $(".conversation-group[data-id='"+msg.conversationId+"']").click(function(){
					selectConversationTarget($(this).attr("data-id"));
				});
			
                
				setConversationGroupDisplay(msg.conversationId);
			}
			else { // equal 1
				$(".conversation-group[data-id='"+msg.conversationId+"']").prependTo(".conversations-list");
				setConversationGroupDisplay(msg.conversationId,1);
				
				msg.targetName = $("#conversation-name-"+msg.conversationId+" div").text();
				msg.targetPhoto = $("#conversation-img-"+msg.conversationId).attr("src");
			}
			
			if(currentConversatoinId == msg.conversationId && $("#messages-loading").css('display') == 'none') {
				var html = '</div><div class="message-wrapper">'+
                        			'<div class="message-target-image"><img src="'+msg.targetPhoto+'"></div>'+
                        			'<div class="messgae-detail">'+
                        				'<div class="message-target-name"><a href="<?php echo Properties::$PROFILE_PAGE ?>?id='+msg.fromId+'">'+msg.targetName+'</a></div>'+
                        				'<div class="message-body">'+
                        					'<div class="message-text">'+
                        						msg.content+
                        					'</div>'+
                        					'<div class="message-time">'+
                        						new Date(msg.createTime).format('hh:nn mmmm dd')+
                        					'</div>'+
                        				'</div>'+
                        			'</div>'+
                        		'</div>';
                $(".messages-list").append(html);
                
				setConversationGroupDisplay(msg.conversationId,0,0);
                
                readMessage(msg.id);
                
				setTimeout(function(){
					$(".messages-list-wrapper").mCustomScrollbar("scrollTo","bottom")
				},100);
			}
		}
		
		function selectConversationTarget(cid) {
			$(".messages-list").html("");
			
			loadMessages(cid, new Date(), function(){
				setTimeout(function(){$(".messages-list-wrapper").mCustomScrollbar("scrollTo","bottom")},100);
			});
			
			$(".conversation-group .conversation-background-wrapper").removeClass("conversation-selected");
			
			var targetConversationHtml = $(".conversation-group[data-id='"+cid+"'] .conversation-background-wrapper");
			targetConversationHtml.addClass("conversation-selected");
			
			
			currentConversatoinId = targetConversationHtml.attr("data-id");
			currentTargetId = targetConversationHtml.attr("data-target-id");
		}
		
		function setConversationGroupDisplay(cid,offset,fixNum) {
			
   			offset = typeof offset !== 'undefined'? offset : 0;
   			fixNum = typeof fixNum !== 'undefined'? fixNum : -1;
			
            window.setTimeout(function(){
            	var numOfUnreadMsg = parseInt($("#unread-msg-num-"+cid).text());
            	numOfUnreadMsg = numOfUnreadMsg+offset;
            	
            	if(fixNum != -1) {
            		numOfUnreadMsg = fixNum;
            	}
            	
            	$("#unread-msg-num-"+cid).text(numOfUnreadMsg);
            		
				if(numOfUnreadMsg == 0) {
					$("#unread-msg-num-"+cid).hide();
					$("#conversation-name-"+cid+" div").css("font-weight",600);
				}
				else {
					$("#unread-msg-num-"+cid).show();
					$("#conversation-name-"+cid+" div").css("font-weight",800);
				}
            },100);
		}
		
		function loadConversations(time, callback) {
			$("#conversations-loading").show();
			
			$.ajax({
                type: "GET",
                url: "<?php echo Properties::$MESSAGE_GET_CONVERSATIONS_ACTION ?>",
                dataType: "json",
                success: function(data) {
	                for(var i=0; i<data.length; i++) {
						
						var imgUrl = null;
						if(data[i].targetPhoto==null)
							imgUrl = "../imgs/profile-default.png"
						else
						   imgUrl = data[i].targetPhoto.linkSmall;
						
						var html = '<div  class="conversation-group" data-target-id="'+data[i].targetUserId+'" data-id="'+data[i].id+'">'+
	                           		'<div data-id="'+data[i].id+'" data-target-id="'+data[i].targetUserId+'" class="conversation-background-wrapper">'+
	                           		'<div class="conversation-pic">'+
	                                       '<div class="align-marker"></div><img id="conversation-img-'+data[i].id+'" src="'+imgUrl+'">'+
	                                   '</div>'+
	                                   '<div id="conversation-name-'+data[i].id+'" data-id="'+data[i].id+'"  data-target-id="'+data[i].targetUserId+'" class="conversation-name"><div>'+
	                    	               	data[i].targetName +
	                                   '</div></div>'+
						 	   	       '<div id="unread-msg-num-'+data[i].id+'" class="unread-msg-num" style="display:none">'+data[i].targetCountUnread+'</div>' +
	                                   '</div>'+
	                            	'</div>';
	                    $(".conversations-list").append(html);
	                     
	                    setConversationGroupDisplay(data[i].id);
					}
					
					if(data.length != 0) {
						$("#conversations-loading").hide();
					}
					else {
						$("#conversations-loading").text("You don't have any contacts yet.")
					}
				
					$(".conversation-group").click(function(){
						selectConversationTarget($(this).attr("data-id"));
					});
					
					callback();
                }
            });
             
		}
		
		function initConvesationList() {
			$(".conversations-list-wrapper").mCustomScrollbar({
			    	theme:"minimal-dark"
			});
			
			$(".messages-list-wrapper").mCustomScrollbar({
			    	theme:"minimal-dark"
			});
			
			$("#conversations-loading").show();
			$("#messages-loading").show();
			
			loadConversations(new Date(), function(){
				if($(".conversation-group").length == 0) {
					//TODO: show no conversation and message msg
					return;
				}
				
				var lastConvEle = $(".conversation-group .conversation-background-wrapper").first();
				lastConvEle.addClass("conversation-selected");
				
				currentConversatoinId = lastConvEle.attr("data-id");
				currentTargetId = lastConvEle.attr("data-target-id");
				
				loadMessages(currentConversatoinId, new Date(), function(){
					setTimeout(function(){$(".messages-list-wrapper").mCustomScrollbar("scrollTo","bottom")},100);
				});
			});
			
			
			
		}
		
		function loadMessages(cid,time, callback) {
			$("#messages-loading").show();
			
			requestData = {conversationId:cid};
			
			var targetName = $('#conversation-name-'+cid+' div').text();
			var targetId = $('#conversation-name-'+cid).attr("data-target-id");
			var targetImg = $('#conversation-img-'+cid).attr("src");
			var myName = "<?php echo $me->firstName() ?>";
			var myId = "<?php echo $me->id ?>";
			var myPic = "<?php echo $mMainPicLink ?>";
			
			$.ajax({
                type: "GET",
                url: "<?php echo Properties::$MESSAGE_GET_MESSAGES_ACTION ?>",
                data: requestData,
                dataType: "json",
                success: function(data) {
                	
	                for(var i=0; i<data.length; i++) {
	                	
	                	var name, pic;
	                	if(data[i].fromMe == 1) {
	                		name = myName;
	                		pic = myPic;
	                		userId = myId;
	                	}
	                	else {
	                		name = targetName;
	                		pic = targetImg;
	                		userId = targetId;
	                	}
	                	
						// convert messages to html
					 	var html = '</div><div class="message-wrapper">'+
                        			'<div class="message-target-image"><img src="'+pic+'"></div>'+
                        			'<div class="messgae-detail">'+
                        				'<div class="message-target-name"><a href="<?php echo Properties::$PROFILE_PAGE ?>?id='+userId+'">'+name+'</a></div>'+
                        				'<div class="message-body">'+
                        					'<div class="message-text">'+
                        						data[i].content+
                        					'</div>'+
                        					'<div class="message-time">'+
                        						new Date(Number(data[i].createTime)).format('hh:nn mmmm dd')+
                        					'</div>'+
                        				'</div>'+
                        			'</div>'+
                        		'</div>';
                     	$(".messages-list").prepend(html);
					}
					
					$("#messages-loading").hide();
					
					readConversation(cid);
					callback();
	            }
            });
			
		}
		
        $(document).ready(function() {
			
			initConvesationList();
			var pusher = new Pusher('<?php echo Properties::$PUSHER_KEY ?>',{ authEndpoint: "<?php echo Properties::$PUSHER_AUTH_ACTION ?>" });
			var msgChannel = pusher.subscribe("private-msg-"+myId);
			
			msgChannel.bind("new-message", function(data) {
    			receiveNewMessage(data);
  			});
  			
  			$("#send-btn").click(function(){
  				if($("#send-btn").hasClass("processing")) {
  					return false;
  				}
  				
  				var content = $("#input-message").val();
  				if(content=="" || content==null) {
            		$("#error-box").show();
            		$("#error-box").text("Please enter your message.");
            		$("#error-box").delay(2000).fadeOut(1000);
            		return false;
  				}
  				
  				$("#send-btn").addClass("processing");
  				$("#send-btn").text("Sending..");
  				
  				sendMessage(content, function(){
  					$("#send-btn").removeClass("processing");
  					$("#input-message").val("");
  					$("#send-btn").text("Send");
  				});
  				
  				return false;
  			});
		});
	</script>
	<script>
		selectTab("#message-tab");
	</script>
	
</html>