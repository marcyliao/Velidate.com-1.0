<?php
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/PictureService.class.php";
    require_once __DIR__."/../../service/SearchService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    try {

	   
	    require "component/userAuth.php";
	    require "component/userProfilePic.php";
		
		
		$now = new DateTime();
		$birth = new DateTime($me->birthday);
		$age = $now->format('Y') - $birth->format('Y');
		
		$from = ($age-5 >= 18) ? ($age-5) : 18;
		$to = ($age+5 <= 99) ? ($age+5 ) : 99;


	}
	catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
        <link rel="stylesheet" href="../libs/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="../libs/owl-carousel/owl.theme.css">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div id="message-box" class="alert alert-info"></div>
        <div class="site-wrapper for-footer-header">
            <div class="site-wrapper-inner">
                <?php require "component/navigation.php"; ?>
                <div class="main-content">
                    <div class="content-container">
                        <div class="search-column">
                            <div class="search-criteria">
                                <h2 class="search-section-title">Search</h2>
                                <div class="criteria-panel">
                                    <div class="criteria-group">
                                        <div class="criteria-title">Sex</div>
                                        <div>
                                            <select id="gender" class="form-control">
                                                <option value="1"  >Male</option>
                                                <option value="2"  >Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="criteria-group">
                                        <div class="criteria-title">Age</div>
                                        <div class="criteria-age">
                                            <select id="from" class="form-control">
                                            </select>
                                            to
                                            <select id="to" class="form-control">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="criteria-group">
                                        <div class="criteria-title">City</div>
                                        <div>
                                            <input type="text" class="form-control" id="city" placeholder="City" value="<?php echo $me->city ?>">
                                        </div>
                                    </div>
                                    <div class="criteria-group">
                                        <div class="criteria-title">Country</div>
                                        <div>
                                            <select id="country" class="form-control country-criteria">
                                                <option value="Canada" >Canada</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="criteria-group search-btn">
                                        <a id="searchBtn" href="" class="button_s blue">search</a>
                                    </div>
                                </div>
                            </div>
                            <div class="search-result-group">
                                <h2 id="result-title" class="search-section-title">Results</h2>
                                <div class="search-results">
                                    <table id="result-table">
                                    </table>
                                    <div id="no-result" class="alert alert-info" >No results found..</div>
                                    <div style="text-align:center">
                                        <ul id="pager" class="pagination">
                                          <li id="prev-page"><a href="#">&laquo;</a></li>
                                          <li id="current-page"><a href="#">1</a></li>
                                          <li id="next-page"><a href="#">&raquo;</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <script src="../libs/owl-carousel/owl.carousel.js"></script>
    <script>
        var page = 0;
        var resultsCount = 0;

        function updatePager(pageTmp) {
            page = pageTmp;
            if(page == 0){
                $("#prev-page").addClass("disabled");
            }
            else{
                $("#prev-page").removeClass("disabled");
            }

            if(resultsCount < <?php echo SearchService::NUM_PER_PAGE?>){
                $("#next-page").addClass("disabled");
            }
            else{
                $("#next-page").removeClass("disabled");
            }

            if(resultsCount != 0) {
                $("#current-page a").text(page+1);
            }
        }

        function load(pageTmp) {

            $("#message-box").text("Loading...");
            $("#message-box").show();

            data = {}
            data["page"] = pageTmp;

            if($("#gender").val() != null && $("#gender").val() != "")
                data["gender"] = $("#gender").val();

            if($("#city").val() != null && $("#city").val() != "")
                data["city"] = $("#city").val();

            if($("#country").val() != null && $("#country").val() != "")
                data["country"] = $("#country").val();

            if($("#from").val() != null && $("#from").val() != "") {
                var d = new Date();
                var fromYear = d.getFullYear()+1-parseInt($("#from").val());
                var fromMonth = d.getMonth()+1;
                var fromDate = d.getDate();

                data["to"] = fromYear+"-"+fromMonth+"-"+fromDate;

            }

            if($("#to").val() != null && $("#to").val() != "") {
                var d = new Date();
                var toYear = d.getFullYear()-parseInt($("#to").val());
                var toMonth = d.getMonth()+1;
                var toDate = d.getDate();

                data["from"] = toYear+"-"+toMonth+"-"+toDate;
            }
			
			
			window.location.hash = JSON.stringify(data); 
            $.ajax({
                type: "GET",
                url: "<?php echo Properties::$SEARCH_ACTION ?>",
                dataType: "json",
                data: data,
                success: function(data) {
                    $("#loading").hide();

                    var table = $("#result-table");

                    var htmlItem = ""
                    if(data.length != 0) {
                        $("#no-result").hide();

                        htmlItem = '<tr><th></th><th>Name</th><th>Age</th><th></th><th>Profession</th><th></th></tr>';
                        for(var i=0; i<data.length; i++) {
                            var picUrl = data[i].picUrl;
                            if(picUrl == null)
                                picUrl = "../imgs/profile-default.png";
                                
                            var checkHtml = '<img class="search-check" src="../imgs/check.png">';
                            var imgCheck = (data[i].imgCheck? checkHtml: "");
                            var ageCheck = (data[i].ageCheck? checkHtml: "");
                            var professionCheck = (data[i].professionCheck? checkHtml: "");
                                
                            htmlItem +='<tr class="result-item">'
                                +'<td class="search-photo"><a href="<?php echo Properties::$PROFILE_PAGE ?>?id='+data[i].id+'"><img src="'+picUrl+'"></a><div class="check-mark">'+imgCheck+'</div></td>'
                                +'<td class="search-name">'+data[i].firstName+'</td>'
                                +'<td class="search-age">'+(new Date().getFullYear()-new Date(data[i].birthday).getFullYear())+'</td>'
                                +'<td class="search-age-verify">'+ageCheck+'</td>'
                                +'<td class="search-profession">'+data[i].profession+'</td>'
                                +'<td class="search-profession-verify">'+professionCheck+'</td>'
                            +'</tr>';

                        }
                        table.html(htmlItem);
                    }
                    else {
                        table.html("");
                        $("#no-result").show();
                    }
                    

                    $("#result-title").show();
                    $("#pager").show();

                    resultsCount = data.length;
                    updatePager(pageTmp);

                    $("#message-box").fadeOut(1000);
                }
            });
        }

        $(document).ready(function() {

            $("#loading").hide();
            $("#result-title").hide();
            $("#pager").hide();
            $("#no-result").hide();


            for(var i=18; i<=99; i++) {
                 $('#from')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }

            for(var i=18; i<=99; i++) {
                 $('#to')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }
            
            if(window.location.hash != "") {
            	data = JSON.parse(window.location.hash.replace("#",""))
	            page=data.page;
	            $("#country").val(data.country);
	            $("#city").val(data.city);
	            $("#gender").val(data.gender);
	            $('#from').val(new Date().getFullYear()-new Date(data.to.replace(/-/g,"/")).getFullYear()+1);
	            $('#to').val(new Date().getFullYear()-new Date(data.from.replace(/-/g,"/")).getFullYear());
            }
            else {
	            page=0;
	            $("#country").val("<?php echo $me->country ?>");
	            $("#gender").val("<?php echo $me->seeking ?>");
	            $('#from').val(<?php echo $from ?>);
	            $('#to').val(<?php echo $to ?>);
            }

          $("#searchBtn").click(function() {
            page = 0;
            load(page);
            return false;
          });

          $("#current-page a").click(function() {

            load(page);
            return false;
          });

          $("#prev-page a").click(function() {
            if($("#prev-page").hasClass("disabled"))
                return false;

            load(page-1);
            return false;
          });

          $("#next-page a").click(function() {
            if($("#next-page").hasClass("disabled"))
                return false;

            load(page+1);
            return false;
          });

          load(page);
         
        });
    </script>
	<?php require "component/messageTab.php" ?>
	<script>
		selectTab("#search-tab");
	</script>

</html>