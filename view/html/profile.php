<?php
require_once __DIR__ . "/../../service/ContactService.class.php";
require_once __DIR__ . "/../../service/AccountService.class.php";
require_once __DIR__ . "/../../service/PictureService.class.php";
require_once __DIR__ . "/../../service/MessageService.class.php";
require_once __DIR__ . "/../../utility/Utility.class.php";
require_once __DIR__ . "/../../utility/Properties.class.php";

try {
	require "component/userAuth.php";
	require "component/userProfilePic.php";

	$uid = null;
	if (isset($_GET['id'])) {
		$uid = $_GET['id'];
	} else {
		$uid = $mid;
	}

	$user = $accountService -> load($uid);
	$contact = $contactService -> loadByUserId($user -> id);
	$picVerified = $contact -> mainPhotoVerified;
	$picTotal = $picVerified + $contact -> mainPhotoNotVerified;
	$ageVerified = $contact -> ageVerified;
	$ageTotal = $ageVerified + $contact -> ageNotVerified;
	$proVerified = $contact -> professionVerified;
	$proTotal = $proVerified + $contact -> professionNotVerified;
	$verNumber = max($picTotal, $ageTotal, $proTotal);

	$mainPicLink = "../imgs/profile-default.png";
	if ($contact -> mainPhotoId != null) {
		$mainPicLink = $pictureService -> getMediumLink($contact -> mainPhotoId);
	}

	$now = new DateTime();
	$birth = new DateTime($user -> birthday);
	$age = $now -> format('Y') - $birth -> format('Y');

} catch(Exception $e) {
	Utility::redirect(Properties::$MESSAGE_PAGE . "?msg=" . $e -> getMessage(), false);
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
    	<script src="../libs/jquery-1.11.1.min.js"></script>
    	<script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    	<script src="../libs/bootbox.min.js"></script>
    	<link href="../css/font-awesome.css" rel="stylesheet">
    	<link href="../css/bootstrap-social.css" rel="stylesheet">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div id="message-box" class="alert alert-info" style="display:none"></div>
        <div id="error-box" class="alert alert-danger" style="display:none"></div>
        <div class="site-wrapper for-footer-header">
            <div class="site-wrapper-inner">
                <?php
				require "component/navigation.php";
 				?>
                <div class="main-content">
                    <div class="content-container">
                        <div class="column-personal">
                            <div class="profile-pic-container">
                                <a href="<?php  echo Properties::$PHOTOS_PAGE . "?id=" . $user -> id; ?>"><img class="profile-pic" src="<?php echo $mainPicLink ?>"></a>
                            </div>
                            <div class="name-and-age gradient">
                            	<span id="name-and-age-text"><?php echo htmlspecialchars($user->firstName()) ?>, <?php echo $age ?></span> 
                            	<?php if($contact->averageRating() != "N/A") {?>
                            	<span id="rating-number-wrapper"><div class="align-marker"></div><div id="rating-number"><div id="rating-number-actual"><?php echo $contact->averageRating() ?></div><div id="rating-word">rating</div></div></span>
                            	<?php }?>
                            </div>
                            <table class="info-table ">
                                <tr>
                                    <td class="title">Gender:</td><td class="info"><?php echo htmlspecialchars($user->getGender()) ?></td>
                                </tr>
                                <tr>
                                    <td class="title">Seeking:</td><td class="info"><?php echo htmlspecialchars($user->getSeeking()) ?></td>
                                </tr>
                                <tr>
                                    <td class="title">Intent:</td><td class="info"><?php echo htmlspecialchars($user->intent) ?></td>
                                </tr>
                                <tr>
                                    <td class="title">Profession:</td><td class="info"><?php echo htmlspecialchars($user->displayProfession()) ?></td>
                                </tr>
                                <tr>
                                    <td class="title">Location:</td><td class="info"><?php echo htmlspecialchars($user->city.",".$user->country) ?></td>
                                </tr>
                                <tr>
                                    <td class="title">Ethnicity:</td><td class="info"><?php echo htmlspecialchars($user->ethnicity)?></td>
                                </tr>
                                <tr>
                                    <td class="title">Religion:</td><td class="info"><?php echo htmlspecialchars($user->religion)?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="column-rating">
                            <!--<h2><?php echo htmlspecialchars($user->firstName()) ?>'s Profile</h2>-->
                            <h2 class="trustworthy">How Trustworthy This Profile Is</h2>
                             <div class="verification-table-main profile-page-2" style="display:none">
                                <div class="verification-table-wrapper"">
                                  <table border="0" style="height:168px; color:#757f85; font-size:15px;">
                                    <tr>
                                      <td width="160" height="41"><div class="verified-content">Number of people verfied as REAL</div></td>
                                      <td width="352">&nbsp;</td>
                                      <td width="160"><div class="verified-content">Total number of people verified</div></td>
                                    </tr>
                                    <tr>
                                      <td><div align="center"><?php echo $picVerified ?></div></td>
                                      <td><img src= "../imgs/picture-rate.png" width="354" alt="picture"></td>
                                      <td><div align="center"><?php echo $picTotal ?></div></td>
                                    </tr>
                                    <tr>
                                      <td><div align="center"><?php echo $ageVerified ?></div></td>
                                      <td><img src="../imgs/age-rate.png" width="354"  alt="picture"></td>
                                      <td><div align="center"><?php echo $ageTotal ?></div></td>
                                    </tr>
                                    <tr>
                                      <td><div align="center"><?php echo $proVerified ?></div></td>
                                      <td><img src="../imgs/profession-rate.png" width="354"  alt="picture"></td>
                                      <td><div align="center"><?php echo $proTotal ?></div></td>
                                    </tr>
                                  </table>
    							</div>                       
	                            <div class="verified-num"><?php echo $verNumber ?></div>
	                            <div class="verified-content">People Verified this profile</div>
                                <div class="go-back-button-wrapper">
                                	<a id="go-back-btn" class="button button_m blue" href="#">Go Back</a>
                                </div>	
                            </div>
                            <div class="profile-progress profile-page-1">
                                <div class="bar-container"><div class="left-num">0</div><div class="bar-background"><div class="remained-perc" style="width: <?php echo (100-$contact->verificationPercentage())*0.98 ?>%"></div></div><div class="right-num">100</div></div>
                            </div>

                            <div class="verified-num profile-page-1"><a id="button-profile-page2" href="#"><?php echo $verNumber ?></a></div>
                            <div class="verified-content profile-page-1">People Verified this profile</div>

                            <div class="rating-main profile-page-1">
                                <div class="verified-info">
                                	<div class="verified-item"><div style="height:23px"><div class="title-name">
                                    	<?php if($uid == $mid || $user->openFacebookLink ==1) {?>
                                    	<button onclick="window.location.href='https://www.facebook.com/profile.php?id=<?php echo $user->fbId?>'" type="button" class="btn btn-xs btn-social btn-facebook form-component"><i class="fa fa fa-facebook"></i>Facebook</button>
                                    	<br>
                                    	<?php } else {?>
                                    	<strong>Facebook</strong>
                                    	<?php }?>
                                    </div><div class="check-or-get-verified">
                                    	<a class="button_ss blue get-verified" href="#" ><img src="../imgs/check.png"> Verified</a>
									</div></div>
                               
                                    	<?php if($uid == $mid) {?>
	                                    <div style="margin-top:10px">
	                                    	<?php if($me->openFacebookLink == 0) {?>
	                                    		<a id="facebook-setting" data-visibility="0" class="button_ssss grey" style="width:90px; display:block; text-align: center;" href="#">Private</a>
	                                    	<?} else { ?>
	                                    		<a id="facebook-setting" data-visibility="1" class="button_ssss blue" style="width:90px; display:block; text-align: center;" href="#">Public</a>
	                                    	<?} ?>
	                                    </div>
	                                    <script>
	                                    												$("#facebook-setting").click(function() {
            									input = {};
	                                    		input["open-facebook-link"] = 1-$("#facebook-setting").attr("data-visibility");
	                                    		
		                                    	$.ajax({
									                type: "POST",
									                data: input,
                									url: "<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>
														",
														success: function(data) {
														var note = "<h3>Update Complete</h3>"

														changedSetting = input["open-facebook-link"];
														$("#facebook-setting").attr("data-visibility",changedSetting);
														if(changedSetting == 1) {
														$("#facebook-setting").removeClass("grey");
														$("#facebook-setting").addClass("blue");
														$("#facebook-setting").text("Public");
														note+='<p>Your facebook button is "Public" now. You are giving people chance to see your facebook profile. This might increase your chance to get more dates. </p>'
														}
														else {
														$("#facebook-setting").removeClass("blue");
														$("#facebook-setting").addClass("grey");
														$("#facebook-setting").text("Private");
														note+='<p>Your facebook button is "Private" now. You are restricting people to see your facebook profile. </p>'
														}

														bootbox.alert(note);
														},
														error: function(xhr, status, error) {
														$("#error-box").show();
														$("#error-box").text(xhr.responseText);
														$("#error-box").delay(2000).fadeOut(1000);

														}
														});

														return false;
														});
	                                    </script>
	                                    <?php } ?>
                                    </div>
                                    <div class="verified-item"><div class="title-name"><strong>Profile Picture</strong></div><div class="check-or-get-verified">
                                    	<?php if($contact->isPictureVerified()) {?>
                                    	<a class="button_ss blue get-verified" href="#" ><img src="../imgs/check.png"> Verified</a>
                                    	<?php } else { ?>
                                    	<a class="button_ss grey get-verified" href="#" >Not Verified</a>
                                    	<?php } ?>
                                    </div></div>
                                    <div class="verified-item"><div class="title-name"><strong>Profession</strong></div><div class="check-or-get-verified">
                                    	<?php if($contact->isProfessionVerified()) {?>
                                    	<a class="button_ss blue get-verified" href="#" ><img src="../imgs/check.png"> Verified</a>
                                    	<?php } else { ?>
                                    	<a class="button_ss grey get-verified" href="#"  >Not Verified</a>
                                    	<?php } ?>
                                    </div></div>
                                    <div class="verified-item"><div class="title-name"><strong>Age</strong></div><div class="check-or-get-verified">
                                    	<?php if($contact->isAgeVerified()) {?>
                                    	<a class="button_ss blue get-verified" href="#" ><img src="../imgs/check.png"> Verified</a>
                                    	<?php } else { ?>
                                    	<a class="button_ss grey get-verified" href="#">Not Verified</a>
                                    	<?php } ?>
                                    </div></div>
                                    <?php if($user->profession=="student") {?>
                                    <div class="verified-item">
                                    	<div class="title-name"><strong>Student Email</strong></div>
                                    	<div class="check-or-get-verified">
                                    		<?php if($user->studentEmailVerified==1) { ?><a class="button_ss blue get-verified" href="#" ><img src="../imgs/check.png"> Verified</a> <?php } ?>
                                    		<?php if($user->studentEmailVerified==0 && $uid == $mid) {
                                    			$msg = "A verification request has been sent to ".$user->studentEmail
            											.". Please click the link inside to verify your student email."
            											."<br>Still no email recieved? Check your spam or click <a href='".Properties::$RESEND_STUDENT_CONFIRMATION_ACTION."?id=".$mid."'>Here</a> to recieve the email again."
														."<br>Want to change the email address? Go to <a href='".Properties::$EDIT_PROFILE_PAGE_RELATIVE."'>Edit Page</a> to reset your student email."; 
                                    		?><a class="button_ss grey" href="<?php echo Properties::$MESSAGE_PAGE_RELATIVE."?msg=".$msg ?>">Not Verified</a> <?php } ?>
                                    	</div>
                                    </div>
                                    <?php } ?>
                                    
                                </div>
                                <div class="contact-info">
                                	
                        			<?php if($uid != $mid) { ?>
	                        			<?php if($user->gender == 1) {?>
	                                		<h2>Send Him a Message Now</h2>
	                                	<?php } else if ($user->gender == 2) { ?>
	                                		<h2>Send Her a Message Now</h2>
	                                	<?php } ?>
                                	<div>
                                		<div class="profile-message-input-group">
			                            	<textarea id="profile-input-message" class="form-control" placeholder="Message"></textarea>
			                            	<div class="profile-send-button-wrapper">
			                            		<a id="profile-send-btn" class=" button_ss blue" href="#">Send Now</a>
			                            	</div>
			                            </div>
                                	</div>
                                	<script>
                                		function sendMessage(content,callback) {
											$.ajax({
								                type: "GET",
								                data: {toId:<?php echo $uid?>
													, content: content},
													url: "<?php echo Properties::$MESSAGE_SEND_MESSAGE_ACTION ?>",
													success: function(data) {
													callback();
													}
													});
													}

													$("#profile-send-btn").click(function() {
													if ($("#profile-send-btn").hasClass("processing")) {
													return false;
													}

													var content = $("#profile-input-message").val();
													if (content == "" || content == null) {
													$("#error-box").show();
													$("#error-box").text("Please enter your message.");
													$("#error-box").delay(2000).fadeOut(1000);
													return false;
													}

													$("#profile-send-btn").addClass("processing");
													$("#profile-send-btn").text("Sending..");

													sendMessage(content, function() {
													window.location.replace("<?php echo Properties::$CONVERSATION_PAGE ?>");
												});

												return false;
												});
								</script>
                                	<?php } else { ?>
                                    <h2 style="font-size:17px;">Rate Your friends Anonymously</h2>
                                    <div id="ratees-msg" class="contact-group contact-group-msg">
                                    	Loading your contact list...
                                    </div>
                                    
                                    <div id="ratees-list" style="display:none">
                                    	<div id="ratees-ask-login" class="get-contact-btn-group" style="display:none;">
                                    		<div class="contact-group-msg">Login to social network to rate friends</div>
                                    		<div class="by-facebook"><button onclick="window.location.href='<?php echo Properties::$GET_FACEBOOK_USER_ACTION ?>'" type="button" class="btn btn-block btn-social btn-facebook form-component"><i class="fa fa fa-facebook"></i>Rate Facebook Friends</button></div>
                                    		<div class="by-gmail"><button onclick="window.location.href='<?php echo Properties::$GET_GMAIL_USER_ACTION ?>'" type="button" class="btn btn-block btn-social btn-google-plus form-component"><i class="fa fa fa-google-plus"></i>Rate Gmail Contacts</button></div>
                                    	</div>
                                    	<div id="list-wrapper">
                                    	</div>
                                    </div>
                                    
                                	<?php } ?>
                                </div>
                            </div>

                            <div class="about-me">
                            	<?php if( $user->aboutMe !=null && $user->aboutMe != ""){ ?>
                                <div class="about-me-title">About Myself</div>
                                <div class="about-me-content"><?php echo htmlspecialchars($user->aboutMe) ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>
    </body>
    <script>
    	var ratees = null;
    	var ratees_current_index = 0;
    	var ratees_number = 4;
    	var ratees_size = 0;
    	
    	function updateRateesList() {
    		ratees.gmail_available = true //skip gmail ratees by now
    		
        	$("#ratees-msg").hide();
        	if(ratees.facebook_available == false || ratees.gmail_available == false){
        		$("#ratees-ask-login").show();
        		
            	if(ratees.facebook_available == false) {
            		$(".by-facebook").show();
            		ratees_number = 2;
            	}
            	else {
            		$(".by-facebook").hide();
            	}
            	
            	if(ratees.gmail_available == false) {
            		$(".by-gmail").show();
            		ratees_number = 2;
            	}
            	else {
            		$(".by-gmail").hide();
            	}
        	}
        	else{
        		$("#ratees-ask-login").hide();
        	}
                
        	if(ratees_size==0) {
        		$("#list-wrapper").hide();
        		return;
        	}
    		
        	html = "";
        	for(var i=ratees_current_index; i<ratees_size && i<ratees_current_index+ratees_number; i++) {
        		var image = (ratees.ratees[i].photoLinkMedium == null? "../imgs/profile-default.png":ratees.ratees[i].photoLinkSmall);
        		
        		html += '<div id="" class="ratee-item">'
                            +'<div class="contact-pic">'
                                +'<img src="'+image+'">'
                            +'</div>'
                            +'<div class="contact-opt">'
                                +'<div class="contact-name">'
                                    +ratees.ratees[i].name
                                +'</div>'
                                +'<div class="contact-rate-btn">'
                                    +'<a class="button_ss grey" href="<?php echo Properties::$RATING_PAGE_RELATIVE ?>
									#'+i+'">Rate Now!</a>'
									+'</div>'
									+'</div>'
									+'</div>'
									}

									html += '<div id="change-ratees-btn"><a href="#">more...</a></div>';

									$("#ratees-list").show();
									$("#ratees-list #list-wrapper").show();
									$("#ratees-list #list-wrapper").html(html);

									$("#change-ratees-btn").click(function(){
									ratees_current_index+=ratees_number;
									if(ratees_current_index>=ratees_size)
									ratees_current_index = 0;
									updateRateesList();

									return false;
									});

									}

									$(document).ready(function() {
									$('.profile-page-2').hide();
									$('#button-profile-page2').click(function(){
									$('.profile-page-2').fadeIn();
									$('.profile-page-1').hide();
									return false;
									});

									$('#go-back-btn').click(function(){
									$('.profile-page-1').fadeIn();
									$('.profile-page-2').hide();
									return false;
									});
									
            						<?php if($uid == $mid) { ?>
									$('.get-verified').click(function(){
										var note = '<h3 style="text-align:center;">Want to get more verification?</h3>'+
										'<p style="text-align:center;margin:30px 0px;">Simply invite your friends to verify your profile. This will improve your chance to get more dates</p>' +
										'<div style="width:260px;margin-left:auto;margin-right:auto;" class="by-facebook"><button data-bb-handler="ok" onclick="FacebookInviteFriends()" type="button" class="btn btn-block btn-social btn-facebook form-component"><i class="fa fa fa-facebook"></i>Invite your friends to join now!</button></div>'
										bootbox.dialog({
										message: note
										});
									});
									<?php } ?>

            <?php if($uid == $mid) { ?>
            				$("#ratees-msg").text("Loading your contact list...");
            $.ajax({
                type: "GET",
                url: "<?php echo Properties::$RATING_GET_RATEES_ACTION ?>
					",
					dataType: "json",
					success: function(data) {

					ratees = data;
					ratees_size = data.ratees.length;

					updateRateesList();
					$("#ratees-list").show();

					},
					error: function(xhr, status, error) {
					$("#ratees-msg").show();
					$("#ratees-msg").text(xhr.responseText);
					}
					});
            <?php } ?>
				});
    </script>
    <script src="http://connect.facebook.net/en_US/all.js"></script>
	<script>
		FB.init({
			appId : '143547199131394',
			cookie : true,
			status : true,
			xfbml : true
		});

		function FacebookInviteFriends() {
			bootbox.hideAll();

			FB.ui({
				method : 'apprequests',
				message : 'Invite friends to join Velidate.com and rate you.',
				title : 'Invite friends to join Velidate.com and rate you.',
				filters : ['app_non_users']
			});

			return false;
		}

		if (top.location != self.location) {
			top.location = self.location
		}
	</script>
	<?php require "component/messageTab.php" ?>
</html>
