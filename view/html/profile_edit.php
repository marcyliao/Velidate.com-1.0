<?php
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/PictureService.class.php";
    require_once __DIR__."/../../service/SearchService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    try {
         
	    require "component/userAuth.php";
	    require "component/userProfilePic.php";
		
    }
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Velidate</title>
        <link href="../libs/bootstrap-3.1.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/main.css" rel="stylesheet">
        <link href="../css/components.css" rel="stylesheet">
        <link rel="stylesheet" href="../libs/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="../libs/owl-carousel/owl.theme.css">
    </head>

     <body>
        <?php require "component/analyticstracking.php"?>
        <div id="message-box" class="alert alert-info"></div>
        <div id="error-box" class="alert alert-danger"></div>
        <div class="site-wrapper for-footer-header">
            <div class="site-wrapper-inner">
                <?php require "component/navigation.php"; ?>
                <div class="main-content">
                    <div class="content-container">
                        <div class="edit-form-container">
                            <div class="column-editing">
                                <div>
                                    <strong>Essential Information:</strong>
                                </div>
                                 <form id="edit-name-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper"><input type="text" class="inline-edit form-control" name="name" id="edit-name" value="<?php echo $me->userName?>" placeholder="First Name and Last Name"></div>
                                    <div class="edit-button-wrapper"><a id="edit-name-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <form id="edit-birthday-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper">
                                        <select id="date" class="form-control" name="birthDate" style="display:inline-block; width:107px; ">
                                        </select>
                                        <select id="month" class="form-control" name="birthMonth" style="display:inline-block; width:107px; margin-left:10px;">
                                        </select>
                                        <select id="year" class="form-control" name="birthYear" style="display:inline-block; width:107px; margin-left:10px;">
                                        </select>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-birthday-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <form id="edit-interest-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper"> 
                                        <select id="interest"  name="interest" class="form-control">
		                                    <option value="-1">Interested in</option>
		                                    <option value="1">Male</option>
		                                    <option value="2">Female</option>
		                                    <option value="3">Both</option>
		                                </select>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-interest-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                            	<form id="edit-intent-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper"> 
                                        <select id="intent" name="intent" class="form-control">
		                                    <option value="0">I am here to</option>
		                                    <option value="Make new friends">Make new friends</option>
		                                    <option value="Chat">Chat</option>
		                                    <option value="Date">Date</option>
		                                </select>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-intent-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <form id="edit-profession-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper">
                                    	<label style="font-weight:100; margin-left:5px;">
							      			<input id="isStudent" type="checkbox" disabled="disabled" > Are you a student?
							    		</label>
							    		<div class="profession-group" style="overflow: hidden">
                                    		<input type="text" name="profession" class="form-control" id="profession" placeholder="Profession">
                                    	</div>
                                        <div class="student-group" style="height:0px; overflow:hidden;">
                                            <select id="program" name="program" class="inline-edit form-control">
                                        		<option value="2">Apprenticeship</option>
                                        		<option value="3">Bachelors</option>
                                        		<option value="4">Diploma</option>
                                        		<option value="5">Doctorate PhD</option>
                                        		<option value="6">Masters</option>
                                            </select>
                                    		<input type="text" name="major" class="inline-edit form-control" id="major" placeholder="Major">
                                            <input type="text" name="studentEmail" class="inline-edit form-control" id="studentEmail" placeholder="Student Email">
                                        </div>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-profession-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <div class="edit-form-label">
                                    <strong>Location:</strong>
                                </div>
                                <form id="edit-location-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper"> 
                                        <input type="text" class="inline-edit form-control" name="postal" id="postal" value="<?php echo $me->zipPostalCode ?>" placeholder="Postal/Zip code (Optional)">
                                        <input type="text" class="inline-edit form-control" name="city" id="city" placeholder="city" value="<?php echo $me->city?>">
                                        <select id="country" class="inline-edit form-control" name="country">
                                            <option value="Canada">Canada</option>
                                        </select>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-location-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <div class="edit-form-label">
                                    <strong>About Myself:</strong>
                                </div>
                                <form id="edit-ethnicity-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper">
                                        <select id="ethnicity" name="ethnicity" class="inline-edit  form-control">
		                                    <option value="Asian">Asian</option>
		                                    <option value="Black">Black</option>
		                                    <option value="Caucasian">Caucasian</option>
		                                    <option value="Hispanic">Hispanic</option>
		                                    <option value="Indian">Indian</option>
		                                    <option value="Middle Eastern">Middle Eastern</option>
		                                    <option value="Mixed">Mixed</option>
		                                    <option value="Native American">Native American</option>
		                                    <option value="Others">Others</option>
                                        </select>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-ethnicity-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <form id="edit-religion-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper">
                                        <select id="religion" name="religion" class="form-control">
		                                    <option value="Muslim">Muslim</option>
		                                    <option value="Catholic">Catholic</option>
		                                    <option value="Hindu">Hindu</option>
		                                    <option value="Buddhist">Buddhist</option>
		                                    <option value="Sikh">Sikh</option>
		                                    <option value="Jewish">Jewish</option>
		                                    <option value="Baptist">Baptist</option>
		                                    <option value="Christian">Christian-other</option>
		                                    <option value="Christian">Chinese traditional religion</option>
		                                    <option value="Nonreligious /Agnostic/Atheist">Non-religious/Agnostic/Atheist</option>
		                                    <option value="Others">Others</option>
                                        </select>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-religion-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <form id="edit-about-me-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper">
                                        <textarea id="about-me" name="about-me" class="form-control" rows="6" placeholder="About Me"><?php echo $me->aboutMe ?></textarea>
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-about-me-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                <!--
                                <div class="edit-form-label">
                                    <strong>Change Password:</strong>
                                </div>
                               <form id="edit-password-group" class="form-group edit-form-group" action="<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>" method="post">
                                    <div class="edit-input-wrapper"> 
                                        <input type="password" class="inline-edit form-control" name="current-password" id="current-password" placeholder="Current Password">
                                        <input type="password" class="inline-edit form-control" name="new-password" id="new-password" placeholder="New Password">
                                        <input type="password" class="inline-edit form-control" id="confirm-new-password" placeholder="Confirm New Password">
                                    </div>
                                    <div class="edit-button-wrapper"><a id="edit-password-button" class="button_full button button_ss blue" href="#" style="display:none">Save</a></div>
                                </form>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php require "component/footer.php"?>
    </body>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <script src="../libs/owl-carousel/owl.carousel.js"></script>
    <script src="../libs/jquery-1.11.1.min.js"></script>
    <script src="../libs/placeholder.js"></script>
    <script src="../libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
    <script src="../libs/bootbox.min.js"></script>
    <script>
		function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
        
        var studentEmail = "<?php echo $me->studentEmail?>";

        $(document).ready(function() {
            $('input, textarea').placeholder();
			
			$('#interest').val("<?php  echo $me->seeking ?>");
			$('#intent').val("<?php  echo $me->intent ?>");
			
            $('#profession').val("<?php echo $me->profession ?>");
			if($('#profession').val() == "student") {
				$('.student-group').css('height','150px');
				$('.profession-group').css('height', '0px');
				$('#isStudent').attr('checked', true);
			}
				
            $('#isStudent').change(function(){
                if($('#isStudent').is(":checked")) {
                    $('.student-group').animate({'height':'150px'},500);
					$('.profession-group').animate({'height':'0px'}, 500, function(){
						$('#profession').val("student");
					});
                } 
                else {
                    $('.student-group').animate({'height':'0px'},500);
					$('#profession').val("");
					$('.profession-group').animate({'height':'40px'},500);
                }
            });

            <?php if($me->profession=="student"){?>
            $("#program").val("<?php echo $me->education ?>");
            $("#major").val("<?php echo $me->major ?>");
            $("#studentEmail").val("<?php echo $me->studentEmail ?>");
            <?php } ?>

            $('#ethnicity').val("<?php echo $me->ethnicity ?>");
            $('#religion').val("<?php echo $me->religion ?>");

            for(var i=2002; i>=1920; i--) {
                 $('#year')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }
            $('#year').val(new Date("<?php echo $me->birthday ?>").getFullYear());

            for(var i=1; i<=12; i++) {
                 $('#month')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }
            $('#month').val(new Date("<?php echo $me->birthday ?>").getMonth()+1);

            for(var i=1; i<=31; i++) {
                 $('#date')
                    .append($("<option></option>")
                    .attr("value",i)
                    .text(i));
            }
            $('#date').val(new Date("<?php echo $me->birthday ?>").getDate()+1);

            $('#country').val("<?php echo $me->country ?>");
            
            
            // initialize button effects
            $(".edit-button-wrapper a").hide();
            $(".edit-input-wrapper").find("input, textarea").keyup(function(){
            	$(this).closest(".edit-form-group").find(".edit-button-wrapper a").show();
            });
            $(".edit-input-wrapper").find("select, input").change(function(){
            	$(this).closest(".edit-form-group").find(".edit-button-wrapper a").show();
            });
            
            
            // button click events
            function save(form_data, button) {
            	button.text("Pending...")
            	$.ajax({
                	type: "POST",
                	url: "<?php echo Properties::$PROCESS_PROFILE_EDIT_ACTION ?>",
                	dataType: "text",
                	data: form_data,
                	success: function(data) {
            			
            			button.text("Save");
            			button.hide();
            			
            		    <?php
            				$msg = "A verification request has been sent to ".$me->studentEmail
            					.". Please click the link inside to verify your student email."
            					."<br>Still no email recieved? Check your spam or click <a href='".Properties::$RESEND_STUDENT_CONFIRMATION_ACTION."?id=".$mid."'>Here</a> to recieve the email again."
            			?>
            			
            			if(form_data["studentEmail"] != null && form_data["studentEmail"]!=studentEmail) {
            				note = "<h3>Update Complete..</h3><p><?php echo $msg ?></p>"
            				bootbox.alert(note);
            				
            				studentEmail = form_data["studentEmail"];
            			}
            			else {
            				note = "<h3>Update Complete</h3>"
            				bootbox.alert(note);
            			}
                	},
                	error: function(xhr, status, error) {
            			$("#error-box").show();
            			$("#error-box").text(xhr.responseText);
            			$("#error-box").delay(2000).fadeOut(1000);
            			
            			button.text("Save");
            			button.hide();
					}
            	});
            }
            
            $("#edit-name-button").click(function(){
            	var newName = $("#edit-name").val();
            	if(newName.search(/[^A-Za-z\s]+/) != -1) {
            		$("#error-box").show();
            		$("#error-box").text("Only charset a-z and A-Z is allowed in a name.");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
                }	
                
            	data = {};
            	data["userName"] = newName;
            	save(data, $("#edit-name-button"));
            	return false;
            });
            
            $("#edit-birthday-button").click(function(){
            	var date = $("#date").val();
            	var month = $("#month").val();
            	var year = $("#year").val();
            	
            	data = {};
            	data["birthDate"] = date;
            	data["birthMonth"] = month;
            	data["birthYear"] = year;
            	warning = "<h3>Warning!</h3><p>You will lose your verification of age if you change your birthday. Are you sure to proceed?</p>"
            	bootbox.confirm(warning, function(result) {
            		if(result) {
            			save(data, $("#edit-birthday-button"));
            		}
				}); 
            	return false;
            });
            
            $("#edit-interest-button").click(function(){
            	var newInterest = $("#interest").val();
            	
            	data = {};
            	data["interest"] = newInterest;
            	save(data, $("#edit-interest-button"));
            	return false;
            });
            
            $("#edit-intent-button").click(function(){
            	var newInterest = $("#intent").val();
            	
            	data = {};
            	data["intent"] = newInterest;
            	save(data, $("#edit-intent-button"));
            	return false;
            });
            
            
            $("#edit-profession-button").click(function(){
            	data = {};
            	data["profession"] = $("#profession").val();
            	if(data["profession"] == null || data["profession"] == "") {
        			$("#error-box").show();
            		$("#error-box").text("Please enter a your profession.");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
            	}
            	
            	if(data["profession"] == "student") {
            		data["program"] = $("#program").val();
            		data["major"] =  $("#major").val();
            		data["studentEmail"] =  $("#studentEmail").val();
            		
            		if(data["major"] == null || data["major"] == "") {
            			$("#error-box").show();
	            		$("#error-box").text("Please enter your major.");
	            		$("#error-box").delay(2000).fadeOut(1000);
	                    return false;
            		}
            		
            		if(data["studentEmail"] == null || data["studentEmail"] == "") {
            			$("#error-box").show();
	            		$("#error-box").text("Please enter your student email.");
	            		$("#error-box").delay(2000).fadeOut(1000);
	                    return false;
            		}
            		
            		if(!isEmail(data["studentEmail"])) {
            			$("#error-box").show();
	            		$("#error-box").text("Please enter a correct email.");
	            		$("#error-box").delay(2000).fadeOut(1000);
	                    return false;
            		}
            			
            	}
            	
            	warning = "<h3>Warning!</h3><p>You will lose your verification of profession if you make the change. Are you sure to proceed?</p>"
            	bootbox.confirm(warning, function(result) {
            		if(result) {
            			save(data, $("#edit-profession-button"));
            		}
				});
            	
            	return false;
            });
            
            $("#edit-location-button").click(function(){
            	data = {};
            	data["country"] = $("#country").val();
            	data["postal"] = $("#postal").val();
            	data["city"] = $("#city").val();
            	
            	if(data["city"] == null || data["city"] == "") {
        			$("#error-box").show();
            		$("#error-box").text("Please enter your city.");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
        		}
        	
            	save(data, $("#edit-location-button"));
            	return false;
            });
            
            $("#edit-ethnicity-button").click(function(){
            	data = {};
            	data["ethnicity"] = $("#ethnicity").val();
            	save(data, $("#edit-ethnicity-button"));
            	return false;
            });
            
            $("#edit-religion-button").click(function(){
            	data = {};
            	data["religion"] = $("#religion").val();
            	save(data, $("#edit-religion-button"));
            	return false;
            });
            
            $("#edit-about-me-button").click(function(){
            	data = {};
            	data["about-me"] = $("#about-me").val();
            	save(data, $("#edit-about-me-button"));
            	return false;
            });
            
            $("#edit-password-button").click(function(){
            	data = {};
            	data["password"] = $("#current-password").val();
            	data["new-password"] = $("#new-password").val();
            	var confirmPassword= $("#confirm-new-password").val();
            	
            	if(data["password"] == "") {
        			$("#error-box").show();
            		$("#error-box").text("Please enter your current password");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
            	}
            	
            	if(data["new-password"] == "") {
        			$("#error-box").show();
            		$("#error-box").text("Please enter your new password");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
            	}
            	
            	if(confirmPassword == "") {
        			$("#error-box").show();
            		$("#error-box").text("Please enter your new confirmed password");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
            	}
            	
            	if(data["new-password"].length < 8 || data["new-password"].length > 23) {
        			$("#error-box").show();
            		$("#error-box").text("The length of a password should be between 8 and 23.");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
                }
            	
            	if(data["new-password"] != confirmPassword) {
        			$("#error-box").show();
            		$("#error-box").text("The confirmed password does not match the password.");
            		$("#error-box").delay(2000).fadeOut(1000);
                    return false;
            	}
            	
            	save(data, $("#edit-password-button"));
            	return false;
            });
        });
    </script>
	<?php require "component/messageTab.php" ?>
	<script>
		selectTab("#edit-tab");
	</script>

</html>