<?php
/**
 * Object represents table 'photo'
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */class Photo {
	
	var $id;	var $contactId;	var $verified;	var $notVerified;	var $linkOriginal;	var $linkLarge;	var $linkMedium;	var $linkSmall;
	var $linkBlur;	var $visibility;	var $isMainPhoto;	var $createTime;	var $updateTime;
}
?>