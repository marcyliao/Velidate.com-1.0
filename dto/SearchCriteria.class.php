<?php
/**
 * Object represents search criteria for search feature
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class SearchCriteria {

	var $gender = null;
	var $from = null;
	var $to = null;
	var $city = null;
	var $country = null;
	var $meId = null;

	public function whereSQL() {
		$where = " where id != '" . $this -> meId . "' and gender = '" . $this -> gender . "' ";
		// if($this->from != null) {
		// $where .= "and birthday >= '".$this->from."' ";
		// }
		// if($this->to != null) {
		// $where .= "and birthday <= '".$this->to."' ";
		// }
		// if($this->city != null) {
		// $where .= "and city = '".$this->city."' ";
		// }

		if ($this -> country != null) {
			$where .= "and country = '" . $this -> country . "' ";
		}

		return $where;
	}

	public function orderBy() {
		$orderBy = " Order by city = '" . $this -> city . "' DESC,  birthday <= '" . $this -> to . "' DESC, birthday >= '" . $this -> from . "' DESC ";
		return $orderBy;
	}

}
?>