<?php
/**
 * Object represents table 'conversation'
 *
 * @author: http://phpdao.com
 * @date: 2014-05-07 19:16
 */
class Conversation {

	var $id;
	var $userId1;	var $userId2;
	var $contactId1;
	var $contactId2;	var $createTime;	var $updateTime;

	// field not in db
	var $targetName;
	var $targetContactId;
	var $targetUserId;
	var $targetCountUnread;
	var $targetPhoto;
}
?>