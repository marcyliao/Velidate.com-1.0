<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--
This file is for demo purpose. If you want to place a link in your website say("Login using Google") for users to login and fetch their gmail
contact info, you can provide the link URL in similar way as it has been used over here.
Else alternativel you can directly use the same link and it will be redirected to validate.php
-->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login with gmail</title>
<style type="text/css">
body{background:url(random_grey_variations.png) repeat; }
#wrapper{width:960px; margin:0 auto; margin-top:100px;}
.button{display:block; height:40px; padding:0 20px; width:200px; background:#CCC; text-decoration:none; font:bold 18px Verdana, Geneva, sans-serif; color:#333; line-height:40px; text-align:center; margin-bottom:50px;}
</style>
</head>
<?php include_once "config.php"; ?>
<body>
	<div id="wrapper">
		
		<a href="https://accounts.google.com/o/oauth2/auth?client_id=<?php print_r($clientid); ?>&redirect_uri=<?php print_r($server_url); ?>&scope=https://www.google.com/m8/feeds/&response_type=code" class="button">Login Using Google</a>
	</div>
</body>
</html>