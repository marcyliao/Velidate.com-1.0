<?php
require_once __DIR__ . "/../dao/mysql/ContactMySqlDAO.class.php";
require_once __DIR__ . "/../dao/mysql/RatingMySqlDAO.class.php";
require_once __DIR__ . '/../dto/Contact.class.php';
require_once __DIR__ . '/../utility/Properties.class.php';

class ContactService {

	private $contactDAO;
	private $ratingDAO;

	function __construct() {
		$this -> contactDAO = new ContactMysqlDAO();
		$this -> ratingDAO = new RatingMysqlDAO();
	}

	public function getContactByEmail($email) {
		$contact = $this -> contactDAO -> loadByEmail($email);
		return $contact;
	}

	public function load($id) {
		$contact = $this -> contactDAO -> load($id);
		return $contact;
	}

	public function loadByUserId($userId) {
		return $this -> contactDAO -> loadByUserId($userId);
	}

	public function update($contact) {
		$this -> contactDAO -> update($contact);
	}

	public function loadByFbIdArray($fbIds) {
		return $this -> contactDAO -> loadByFbIdArray($fbIds);
	}

	public function loadByFbIdAndEmailArray($fbIds, $emails) {
		return $this -> contactDAO -> loadByFbIdAndEmailArray($fbIds, $emails);
	}

	public function getAllRatingsInTheLastPeriod($id, $contactIds, $startTime) {
		return $this -> ratingDAO -> getAllRatingsInTheLastPeriod($id, $contactIds, $startTime);
	}

}
?>