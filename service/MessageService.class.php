<?php
require_once __DIR__ . "/../dao/mysql/MessageMySqlDAO.class.php";
require_once __DIR__ . "/../dao/mysql/ConversationMySqlDAO.class.php";
require_once __DIR__ . '/../dto/Message.class.php';
require_once __DIR__ . '/../dto/Conversation.class.php';
require_once __DIR__ . '/../utility/Properties.class.php';
require_once __DIR__ . '/ContactService.class.php';
require_once __DIR__ . '/PictureService.class.php';
require_once __DIR__ . '/AccountService.class.php';

/*
 * Class that operate on table 'country'. Database Mysql.
 *
 * @author: Marcy
 * @date: 2014-05-08
 */
class MessageService {
	private $conversationDAO;
	private $messageDAO;

	function __construct() {

		$this -> conversationDAO = new ConversationMySqlDAO();
		$this -> messageDAO = new MessageMySqlDAO();
	}

	public function getNumOfMessages($from, $to) {
		return $this -> messageDAO -> getNumOfMessages($from, $to);
	}

	function loadConversation($id) {
		$conversation = $this -> conversationDAO -> load($id);
		return $conversation;
	}

	function loadConversationWithTarget($id, $toId) {
		$pictureService = new PictureService();
		$contactService = new ContactService();

		$convers = $this -> loadConversation($id);

		$targetId = $toId;
		$contact = $contactService -> loadByUserId($targetId);
		$convers -> targetContactId = $contact -> id;
		$convers -> targetUserId = $contact -> userId;
		$convers -> targetName = $contact -> firstName();

		$photo = $pictureService -> getMainPhoto($contact -> id);
		$convers -> targetPhoto = $photo;

		return $convers;
	}

	function FindConversation($uid1, $uid2) {
		$convers = $this -> conversationDAO -> loadByUidPair($uid1, $uid2);
		$this -> mdirection = 0;
		if ($convers == null) {
			$convers = $this -> conversationDAO -> loadByUidPair($uid2, $uid1);
			$this -> mdirection = 1;
		}
		return $convers;
	}

	public function CreateNewMessage($fromId, $toId, $message) {
		$convers = $this -> FindConversation($fromId, $toId);

		$accountService = new AccountService();
		$fromUser = $accountService -> load($fromId);
		$toUser = $accountService -> load($toId);

		$newmessage = new Message;
		if ($convers == null) {
			$newconvers = new Conversation;
			$newconvers -> userId1 = $fromUser -> id;
			$newconvers -> userId2 = $toUser -> id;
			$newconvers -> contactId1 = $fromUser -> contactId;
			$newconvers -> contactId2 = $toUser -> contactId;
			$newconvers -> createTime = round(microtime(true) * 1000);
			$newconvers -> updateTime = $newconvers -> createTime;
			$conversID = $this -> conversationDAO -> insert($newconvers);
			$this -> mdirection = 0;

			// if this is the first conversation of the user, give the message required information to return
			$newmessage -> targetName = $fromUser -> firstName();
			$newmessage -> targetContactId = $fromUser -> contactId;
			$newmessage -> targetCountUnread = 1;

			$pictureService = new PictureService();
			$newmessage -> targetPhoto = $pictureService -> getMainPhoto($fromUser -> contactId);

		} else {
			$conversID = $convers -> id;
			$convers -> updateTime = round(microtime(true) * 1000);
			$this -> conversationDAO -> update($convers);
		}

		$newmessage -> fromId = $fromId;
		$newmessage -> toId = $toId;
		$newmessage -> content = $message;
		$newmessage -> conversationId = $conversID;
		$newmessage -> createTime = round(microtime(true) * 1000);
		$newmessage -> updateTime = $newmessage -> createTime;
		$newmessage -> direction = $this -> mdirection;
		$newmessage -> isRead = 0;
		$newmessage -> id = $this -> messageDAO -> insert($newmessage);

		return $newmessage;
	}

	public function readMessage($messageId) {
		$curmessage = $this -> messageDAO -> load($messageId);
		if ($curmessage != null) {
			$curmessage -> isRead = 1;
			$this -> messageDAO -> update($curmessage);
		}

		return $curmessage;
	}

	public function readAllMessagesOfConversation($conversationId, $receiverId) {
		return $this -> messageDAO ->readAllMessagesOfConversation($conversationId, $receiverId);
	}

	public function getConversationList($userId, $startUpdateTime, $length) {
		$conversations = $this -> conversationDAO -> loadByUid($userId, $startUpdateTime, $length);

		$contactService = new ContactService();
		$pictureService = new PictureService();

		foreach ($conversations as $c) {
			$targetId = ($userId == $c -> userId1) ? $c -> userId2 : $c -> userId1;
			$contact = $contactService -> loadByUserId($targetId);
			$c -> targetContactId = $contact -> id;
			$c -> targetUserId = $contact -> userId;
			$c -> targetName = $contact -> firstName();
			$c -> targetCountUnread = $this -> getNumOfUnreadMessages($c -> id, $userId);
			$c -> targetPhoto = $pictureService -> getMainPhoto($contact -> id);
		}
		return $conversations;
	}

	public function getMessageList($conversationId, $startCreateTime, $length) {
		$messages = $this -> messageDAO -> loadByConversationId($conversationId, $startCreateTime, $length);
		$count = count($messages);
		for ($i = 0; $i < $count; $i++) {
			$messages[$i] -> content = htmlspecialchars($messages[$i] -> content);
		}
		return $messages;
	}

	public function getNumOfUnreadMessages($conversationId, $recieverId) {
		return $this -> messageDAO ->getNumOfUnreadMessages($conversationId, $recieverId);
	}

	public function getNumOfAllUnreadMessages($recieverId) {
		return $this -> messageDAO ->getNumOfAllUnreadMessages($recieverId);
	}

}
?>