<?php
require_once __DIR__ . '/../dto/Rating.class.php';
require_once __DIR__ . "/../dao/mysql/ContactMySqlDAO.class.php";
require_once __DIR__ . "/../dao/mysql/RatingMySqlDAO.class.php";
require_once __DIR__ . "/../dao/mysql/PhotoMySqlDAO.class.php";
require_once __DIR__ . "/../service/PictureService.class.php";
require_once __DIR__ . "/../service/AccountService.class.php";
require_once __DIR__ . '/../dto/Contact.class.php';
require_once 'EmailService.class.php';

class RatingService {
	private $contactDAO;

	function __construct() {
		$this -> contactDAO = new ContactMysqlDAO();
		$this -> ratingDAO = new RatingMysqlDAO();

	}

	public function createRating($raterId, &$ratee) {
		$contact = $this -> contactDAO -> load($ratee -> contactId);
		if ($contact != null) {
			$this -> fillContact($contact, $ratee);
			$ratee -> contactId = $contact -> id;
			$this -> contactDAO -> update($contact);
		} else {
			$contact = new contact;
			$contact -> name = $ratee -> name;
			$contact -> birthday = $ratee -> birthday;
			$contact -> profession = $ratee -> profession;
			$contact -> gender = $ratee -> gender;
			$contact -> mainPhotoId = $ratee -> photoId;
			$contact -> ageVerified = 0;
			$contact -> ageNotVerified = 0;
			$contact -> professionVerified = 0;
			$contact -> professionNotVerified = 0;
			$contact -> mainPhotoVerified = 0;
			$contact -> mainPhotoNotVerified = 0;
			$contact -> sumRatingFace = 0;
			$contact -> sumRatingBody = 0;
			$contact -> numRatingFace = 0;
			$contact -> numRatingBody = 0;
			$contact -> numAllVerified = 0;
			$contact -> createTime = round(microtime(true) * 1000);
			$this -> fillContact($contact, $ratee);
			$ratee -> contactId = $this -> contactDAO -> insert($contact);
		}

		// import the main photo
		$photoservice = new PictureService();
		if ($contact -> mainPhotoId == null && $ratee -> photoLinkMedium != null && $ratee -> photoLinkMedium != "") {

			$photo = new Photo;
			$photo -> verified = 0;
			$photo -> notVerified = 0;
			if ($ratee -> photoVerification == 1) {
				$photo -> verified++;
			} else if ($ratee -> photoVerification == 0) {
				$photo -> notVerified++;
			}

			$photo -> linkOriginal = "";
			$photo -> linkLarge = "";
			$photo -> linkMedium = "";
			$photo -> linkSmall = "";
			$photo -> visibility = 0;
			$photo -> isMainPhoto = 1;
			$photo -> contactId = $ratee -> contactId;
			$photoservice -> loadPhotoFromUrl2($ratee -> photoLinkMedium, $photo);

			$contact -> mainPhotoId = $photo -> id;
			$this -> contactDAO -> update($contact);
		} else if ($contact -> mainPhotoId != null) {

			$photo = $photoservice -> load($contact -> id);
			if ($ratee -> photoVerification == 1) {
				$photo -> verified++;
			} else if ($ratee -> photoVerification == 0) {
				$photo -> notVerified++;
			}
			$photoservice -> update($photo);
		}

		$ratingDTO = new Rating;
		$ratingDTO -> isPhotoReal = $ratee -> photoVerification;
		$ratingDTO -> isPofessionReal = $ratee -> professionVerification;
		$ratingDTO -> isAgeReal = $ratee -> ageVerification;
		$ratingDTO -> fromUserId = $raterId;
		$ratingDTO -> targetContactId = $ratee -> contactId;
		$ratingDTO -> ratingFace = $ratee -> faceRating;
		$ratingDTO -> ratingBody = $ratee -> bodyRating;
		$ratingDTO -> createTime = round(microtime(true) * 1000);
		$ratingDTO -> updateTime = round(microtime(true) * 1000);
		//var_dump($this->ratingDTO);
		$this -> ratingDAO -> insert($ratingDTO);

		$sendEmail = new EmailService();
		$responseLink = "Link not yet decided";

		$accountService = new AccountService();
		$rater = $accountService -> load($raterId);
		$sendEmail -> sendRatingEmail($rater, $ratee);

	}

	private function fillContact(&$contact, &$ratee) {
		if ($ratee -> professionVerification == 1) {
			$contact -> professionVerified++;
		} else if ($ratee -> professionVerification == 0) {
			$contact -> professionNotVerified++;
		}
		if ($ratee -> photoVerification == 1) {
			$contact -> mainPhotoVerified++;
		} else if ($ratee -> photoVerification == 0) {
			$contact -> mainPhotoNotVerified++;
		}
		if ($ratee -> ageVerification == 1) {
			$contact -> ageVerified++;
		} else if ($ratee -> ageVerification == 0) {
			$contact -> ageNotVerified++;
		}
		$contact -> numAllVerified++;
		if ($ratee -> faceRating > 0) {
			$contact -> sumRatingFace = $contact -> sumRatingFace + $ratee -> faceRating;
			$contact -> numRatingFace++;
		}
		if ($ratee -> bodyRating > 0) {
			$contact -> sumRatingBody = $contact -> sumRatingBody + $ratee -> bodyRating;
			$contact -> numRatingBody++;
		}
		$contact -> updateTime = round(microtime(true) * 1000);
		if ($contact -> userId == null) {// this is a non-member and we need to update the email
			$contact -> email = $ratee -> email;
		}
		$contact -> fbId = $ratee -> fbId;
	}

}
?>