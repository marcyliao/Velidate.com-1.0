<?php
require_once __DIR__ . "/../dao/mysql/UserMySqlDAO.class.php";
require_once __DIR__ . "/../dao/mysql/ContactMySqlDAO.class.php";
require_once __DIR__ . '/../dto/Contact.class.php';
require_once __DIR__ . '/../utility/password.php';
require_once 'EmailService.class.php';
require_once 'PictureService.class.php';
require_once __DIR__ . '/../utility/Properties.class.php';

/*
 * Class that operate on table 'country'. Database Mysql.
 *
 * @author: Marcy
 * @date: 2014-05-08
 */
class AccountService {

	const FROM_MANUAL = 0;
	const FROM_FACEBOOK = 1;

	private $userDAO;
	private $contactDAO;

	function __construct() {
		$this -> userDAO = new UserMySqlDAO();
		$this -> contactDAO = new ContactMysqlDAO();
	}

	public function update($user) {
		$this -> userDAO -> update($user);
	}

	public function checkIfUserExists($email) {
		$user = $this -> userDAO -> loadByEmail($email);
		if ($user != null) {
			return true;
		}
		return false;
	}

	public function checkIfStudentExists($studentEmail) {
		$size = count($this -> userDAO -> loadByStudentEmail($studentEmail));
		if ($size >= 1) {
			return true;
		}
		return false;
	}

	public function changePassword($id, $oldPassword, $newPassword) {
		$user = $this -> load($id);

		// check password
		$options = array('cost' => 11, 'salt' => $user -> salt);

		$trueCode = password_hash($oldPassword, PASSWORD_BCRYPT, $options);

		if ($user -> passwordHash != $trueCode)
			throw new Exception("The password is not correct");

		if ($oldPassword == $newPassword)
			throw new Exception("The new password is the same as the current correct");

		// create encryped salt
		$user -> salt = uniqid('', true);

		// create hashed password
		$options = array('cost' => 11, 'salt' => $user -> salt);
		$user -> passwordHash = password_hash($newPassword, PASSWORD_BCRYPT, $options);

		$this -> update($user);
	}

	/**
	 * Sign up a user manually
	 *
	 * @param User $user has the information when the user sign up.
	 *        The user will not be registered utill he confirmed in
	 *        his email.
	 * @param String $password the password the user enters. This param is
	 *        necessary because we don't have any field to store user's
	 *        password. A salt and a encryped password will be created and
	 *        then get stored in the database.
	 * @param int $source FROM_MANUAL or FROM_FACEBOOK
	 * @return id of the user
	 */
	public function signUpUser($user, $password, $source) {

		// check whether the user is already registered
		if ($this -> checkIfUserExists($user -> email))
			throw new Exception('The user with the same email is already registered');

		// create encryped salt
		$user -> salt = uniqid('', true);

		// create hashed password
		$options = array('cost' => 11, 'salt' => $user -> salt);
		$user -> passwordHash = password_hash($password, PASSWORD_BCRYPT, $options);

		$user -> createDate = round(microtime(true) * 1000);
		$user -> updateDate = round(microtime(true) * 1000);
		$user -> lastLoginTime = round(microtime(true) * 1000);

		$emailService = new EmailService();
		if ($source == self::FROM_MANUAL) {
			$user -> facebookVerified = 0;
			$user -> accountStatus = User::PENDING;

			$confirmLink = Properties::$CONFIRM_EMAIL_ACTION . "?email=" . $user -> email . "&code=" . $user -> salt;
			$emailService -> sendConfirmation($user -> email, $confirmLink);

			$user -> id = $this -> userDAO -> insert($user);
		} else if ($source == self::FROM_FACEBOOK) {
			$user -> facebookVerified = 1;
			$user -> accountStatus = User::CONFIRMED;

			$user -> id = $this -> userDAO -> insert($user);

			$contact = $this -> createContactOfUser($user);

			$user -> contactId = $contact -> id;
			$user -> mainPhotoId = $contact -> mainPhotoId;
			$this -> userDAO -> update($user);

			if (!session_id()) {
				session_start();
			}
			$userPics = isset($_SESSION['userpics']) ? $_SESSION['userpics'] : null;
			$npics = count($userPics);
			$photoservice = new PictureService();
			for ($i = 0; $i < $npics; $i++) {
				try {
					$pid = $photoservice -> loadPhotoFromUrl($userPics[$i], $contact -> id);
					if ($i == 0) {$photoservice -> changeMainPhoto($contact -> id, $pid);
					}
				} catch(Exception $e) {
					//TODO: catch exception
				}
			}
			
			
			// clear the userpics here
			// to fix the bug that when create fake profiles
			// it creates the pictures in the session
			// signupContinue.php also has this code
			if(isset($_SESSION['userpics'])){
				$_SESSION['userpics'] = null;
				unset($_SESSION['userpics']);
			}	
		}

		if ($user -> profession == "student") {
			$confirmLink = Properties::$CONFIRM_STUDENT_EMAIL_ACTION . "?email=" . $user -> studentEmail . "&code=" . $user -> salt;
			$emailService -> sendStudentConfirmation($user -> studentEmail, $confirmLink);
		}

		return $user -> id;
	}

	/**
	 * Login
	 */
	public function login($email, $password) {

		$user = $this -> userDAO -> loadByEmail($email);
		if ($user == null)
			throw new Exception("The email or password you entered is incorrect");

		// check password
		$options = array('cost' => 11, 'salt' => $user -> salt);
		$trueCode = password_hash($password, PASSWORD_BCRYPT, $options);

		if ($user -> passwordHash != $trueCode)
			throw new Exception("The email or password you entered is incorrect");

		return $user;
	}

	public function loginUser($user) {
		$_SESSION['user_id'] = $user -> id;
		$user -> totalLoginTimes += 1;
		$user -> lastLoginTime = round(microtime(true) * 1000);
		$user -> remindersSent = 0;
		$this -> userDAO -> update($user);
	}

	public function load($id) {
		$user = $this -> userDAO -> load($id);
		return $user;
	}

	public function loadByEmail($email) {
		$user = $this -> userDAO -> loadByEmail($email);
		return $user;
	}

	public function loadByFbId($fbId) {
		$user = $this -> userDAO -> loadByFbId($fbId);
		return $user;
	}

	public function loadByContactId($contactId) {
		$user = $this -> userDAO -> loadByContactId($contactId);
		return $user;
	}

	/**
	 * The logic when receive the user's confirmation from email.
	 */
	public function recieveComfirmationEmail($email, $salt) {

		// check whether the user has
		$user = $this -> userDAO -> loadByEmail($email);
		if ($user == null)
			throw new Exception("No user has this email");

		if ($user -> salt != $salt) {
			throw new Exception("Salt does not match the user");
		}

		$contact = $this -> createContactOfUser($user);
		$user -> contactId = $contact -> id;
		$user -> mainPhotoId = $contact -> mainPhotoId;
		// update the user status in db
		$user -> accountStatus = User::CONFIRMED;

		$this -> userDAO -> update($user);

		return $user;
	}

	public function recieveStudentComfirmationEmail($studentEmail, $salt) {

		// check whether the user has
		$user = $this -> userDAO -> loadByStudentEmail($studentEmail);
		if ($user == null)
			throw new Exception("No user has this student email");

		if ($user -> salt != $salt) {
			throw new Exception("Salt  does not match the user");
		}

		// update the user student email status in db
		$user -> studentEmailVerified = 1;

		$this -> userDAO -> update($user);

		return $user;
	}

	private function createContactOfUser($user) {
		// check whether there is a contact for him, if so, link the contact to the user
		// if not, create a contact and link it to the user, store the contact to db
		$contact = $this -> contactDAO -> loadByFbId($user -> fbId);

		if ($contact != null) {
			$contact -> userId = $user -> id;
			$contact -> email = $user -> email;
			# user email is obtained from facebook
			$contact -> fbId = $user -> fbId;
			$contact -> name = $user -> userName;
			$contact -> profession = $user -> displayProfession();
			$contact -> birthday = $user -> birthday;
			$contact -> gender = $user -> gender;

			$this -> contactDAO -> update($contact);
		} else {
			$contact = new Contact();
			$contact -> email = $user -> email;
			$contact -> fbId = $user -> fbId;
			$contact -> userId = $user -> id;
			$contact -> gender = $user -> gender;
			$contact -> birthday = $user -> birthday;
			$contact -> ageVerified = 0;
			$contact -> ageNotVerified = 0;
			$contact -> profession = $user -> displayProfession();
			$contact -> professionVerified = 0;
			$contact -> professionNotVerified = 0;
			$contact -> mainPhotoVerified = 0;
			$contact -> mainPhotoNotVerified = 0;
			$contact -> createTime = round(microtime(true) * 1000);
			$contact -> updateTime = round(microtime(true) * 1000);
			$contact -> sumRatingFace = 0;
			$contact -> sumRatingBody = 0;
			$contact -> numRatingFace = 0;
			$contact -> numRatingBody = 0;
			$contact -> numAllVerified = 0;
			$contact -> name = $user -> userName;

			$contact -> id = $this -> contactDAO -> insert($contact);
		}
		return $contact;
	}

}
?>