<?php
    
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/InstituteService.class.php";
    require_once __DIR__."/../../dto/User.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";

    try {

        session_start();
        $user = isset($_SESSION['sign-up-form'])? $_SESSION['sign-up-form'] : null;

        if($user == null)
            Utility::redirect(Properties::$MESSAGE_PAGE."?msg=Error..",false);

        if($user->gender == null || $user->gender == -1) {
            $user->gender = intval ($_POST["gender"]);
        }

        if($user->seeking == null || $user->seeking == -1) {
            $user->seeking = intval ($_POST["interest"]);
        }

        if($user->userName == null || $user->userName == "")
            $user->userName = $_POST["username"];

        if($user->country == null || $user->country == "")
            $user->country = $_POST["country"];


        if($user->city == null || $user->city == "")
            $user->city = $_POST["city"];

        if($user->zipPostalCode == null || $user->zipPostalCode == "")
            $user->zipPostalCode = $_POST["postalCode"];

        if($user->birthday == null || $user->birthday == "") {

            $birthDate = $_POST["birthDate"];
            $birthMonth = $_POST["birthMonth"];
            $birthYear = $_POST["birthYear"];
            if($birthDate == null || $birthMonth == null || $birthYear == null) {
                $user->birthday = null;
            }
            else {
                $date = new DateTime();
                $date->setDate($birthYear, $birthMonth, $birthDate);
                $user->birthday = $date->format('Y-m-d');
            }
        }

        if($user->profession == null || $user->profession == "") {
            $user->profession = $_POST["profession"];

            if($user->profession=="student") {
                $user->isStudent = 1;
                $user->studentEmail = $_POST["studentEmail"];
				$instituteService = new InstituteService();
				$user->institute = $instituteService->getSchoolFromEmail($user->studentEmail);
                $user->major = $_POST["major"];
                $user->education = $_POST["program"];
            }
            else {
                $user->isStudent = 0;
            }
        }

        $user->ethnicity= $_POST["ethnicity"];
        $user->religion = $_POST["religion"];
		$user->intent = $_POST["intent"];
        $user->aboutMe = $_POST["about-me"];

        // Velidation
        $valid = true;

        if($user->gender === null || !filter_var($user->gender, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>1, "max_range"=>2)))){
            echo $user->gender;
            $valid = false;
        }

        if($user->seeking === null || !filter_var($user->seeking, FILTER_VALIDATE_INT, array("options"=>array("min_range"=>1, "max_range"=>3)))){
            echo $user->seeking;
            $valid = false;
        }

        if($user->userName == null || $user->userName=="") {
            echo "name";
            $valid = false;
        }
        if($user->birthday == null){
            echo "birthday";
            $valid = false;
        }
        
        if($user->country == null ){
            echo "country";
            $valid = false;
        }

        if($user->city == null ){
            echo "city";
            $valid = false;
        }

        if($user->profession == null){
            echo "pref";
            $valid = false;
        }

        if($user->ethnicity == null) {
            echo "ethnicity";
            $valid = false;
        }

        if($user->religion == null) {
            echo "religion";
            $valid = false;
        }

        if($valid==false)
            throw new Exception("The user parameters are not valid");

        $user->emailNotificationMessages = 1;
        $user->emailNotificationMatches = 1;

        $user->proviceState = "";
        $user->studentEmailVerified = 0;
        $user->numCoins = 20;
		$user->totalLoginTimes = 0;
		$user->openFacebookLink = 1;


        $accountService = new AccountService();
        if($user->facebookVerified == 0) {
            //sign up user
            $id=$accountService->signUpUser($user,$user->password,AccountService::FROM_MANUAL);

            $msg = "Thanks for joining us, ".$user->userName
                ."! A confirmation request has been sent to your email."
                ." Please click the link inside to activate your account."
                ."<br>No email recieved? click <a href='".Properties::$RESEND_CONFIRMATION_ACTION."?id=".$id."'>here</a> to recieve the email again.";

            Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg,false);
        }
        else {
            $user->password = "DEFAULTPASSWORD";
            $id=$accountService->signUpUser($user,$user->password,AccountService::FROM_FACEBOOK);
            $_SESSION['user_id']=$id;
            Utility::redirect(Properties::$PHOTOS_PAGE);
        }


    }
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }

?>