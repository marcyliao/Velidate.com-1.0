<?php
require_once __DIR__ . "/../../service/PictureService.class.php";
require_once __DIR__ . "/../../service/AccountService.class.php";
require_once __DIR__ . "/../../service/ContactService.class.php";
require_once __DIR__ . "/../../service/RatingService.class.php";
require_once __DIR__ . "/../../dto/Ratee.class.php";

session_start();
$uid = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
if ($uid == null) {
	Utility::message(406, "Your session has expired.");
}

$ratees = isset($_SESSION["Ratees"]) ? $_SESSION['Ratees'] : null;
if ($ratees == null) {
	Utility::message(406, "No friends found.");
}

$index = (isset($_POST["index"]) ? (int)($_POST["index"]) : null);
if ($index === null) {
	Utility::message(406, "Parameters error");
}

$ratee = isset($ratees["ratees"][$index]) ? $ratees["ratees"][$index] : null;
if ($ratee == null) {
	Utility::message(406, "Parameters error");
}

$ratee -> professionVerification = (isset($_POST["professionVerification"]) ? $_POST["professionVerification"] : null);
$ratee -> photoVerification = (isset($_POST["photoVerification"]) ? $_POST["photoVerification"] : null);
$ratee -> ageVerification = (isset($_POST["ageVerification"]) ? $_POST["ageVerification"] : null);

$ratee -> faceRating = (isset($_POST["faceRating"]) ? $_POST["faceRating"] : null);
$ratee -> bodyRating = (isset($_POST["bodyRating"]) ? $_POST["bodyRating"] : null);
$ratee -> email = (isset($_POST["rateeEmail"]) ? $_POST["rateeEmail"] : null);

if ($ratee -> profession == null) {
	$ratee -> professionVerification = -1;
}

if ($ratee -> birthday == null) {
	$ratee -> ageVerification = -1;
}

if ($ratee -> photoLinkMedium == null) {
	$ratee -> photoVerification = -1;
}

if ($ratee -> email == null) {
	Utility::message(406, "Parameters error");
}

if ($ratee -> professionVerification == null || ($ratee -> professionVerification != -1 && $ratee -> professionVerification != 0 && $ratee -> professionVerification != 1)) {

	Utility::message(406, "Parameters error");
}

if ($ratee -> photoVerification == null || ($ratee -> photoVerification != -1 && $ratee -> photoVerification != 0 && $ratee -> photoVerification != 1)) {

	Utility::message(406, "Parameters error");
}

if ($ratee -> ageVerification == null || ($ratee -> ageVerification != -1 && $ratee -> ageVerification != 0 && $ratee -> ageVerification != 1)) {

	Utility::message(406, "Parameters error");
}

if ($ratee -> faceRating == null || ($ratee -> faceRating != 1 && $ratee -> faceRating != 2 && $ratee -> faceRating != 3 && $ratee -> faceRating != 4 && $ratee -> faceRating != 5 && $ratee -> faceRating != 6)) {

	Utility::message(406, "Parameters error");
}

if ($ratee -> bodyRating == null || ($ratee -> bodyRating != 1 && $ratee -> bodyRating != 2 && $ratee -> bodyRating != 3 && $ratee -> bodyRating != 4 && $ratee -> bodyRating != 5 && $ratee -> bodyRating != 6)) {

	Utility::message(406, "Parameters error");
}

$ratingService = new RatingService();

try {
	$ratingService -> createRating($uid, $ratee);

	unset($ratees["ratees"][$index]);
	$ratees["ratees"] = array_values($ratees["ratees"]);
	$_SESSION["Ratees"] = $ratees;
} catch(Exception $e) {
	Utility::message(406, $e -> getMessage());
}
?>