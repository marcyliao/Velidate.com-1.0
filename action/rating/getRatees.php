<?php

require_once __DIR__ . "/../../service/fbsrc/facebook.php";
require_once __DIR__ . "/../../utility/Properties.class.php";
require_once __DIR__ . "/../../utility/fbConfig.class.php";
require_once __DIR__ . "/../../service/ContactService.class.php";
require_once __DIR__ . "/../../service/AccountService.class.php";
require_once __DIR__ . "/../../service/PictureService.class.php";
require_once __DIR__ . "/../../dto/Ratee.class.php";
require_once __DIR__ . "/../../dto/Contact.class.php";
require_once __DIR__ . "/../../service/gm/config.php";
require_once __DIR__ . "/../../service/gm/src/Google_Client.php";

session_start();
$mid = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
if ($mid == null) {
	Utility::message(406, "Your session has expired.");
}

$accountService = new AccountService();
$me = $accountService -> load($mid);

$result = null;
if (isset($_SESSION["Ratees"]) && $_SESSION["Ratees"] != null) {
	$result = $_SESSION["Ratees"];
	if ($result["facebook_available"] == true && $result["gmail_available"] == true) {
		echo json_encode($result);
		exit();
	}
}

if ($result == null) {
	$result = array();
	$result["ratees"] = array();
	$result["facebook_available"] = false;
	$result["gmail_available"] = false;
	$_SESSION["Ratees"] = $result;
}

$fbIds = array();
// use to search contacts in database
$gmail_emails = array();
// use to search contacts in database

try {

	$needUpdate = false;
	if ($result["facebook_available"] == false) {
		tryToGetFacebookRatees();
		if ($result["facebook_available"] == true)
			$needUpdate = true;
	}

	if ($result["gmail_available"] == false) {
		tryToGetGmailRatees();
		if ($result["gmail_available"] == true)
			$needUpdate = true;
	}

	if ($needUpdate) {
		replaceByExistingContacts();
		reOederRatees();

		$result["ratees"] = array_slice($result["ratees"], 0, 200);
		$_SESSION["Ratees"] = $result;
	}

	echo json_encode($result);
	exit();

} catch(Exception $e) {
	Utility::message(406, $e -> getMessage());
}

function reOederRatees() {
	global $result;
	$result["ratees"];

	for ($i = 0; $i < count($result["ratees"]); $i++) {

		$result["ratees"][$i] -> orderRating = 0;

		if ($result["ratees"][$i] -> name == null || $result["ratees"][$i] -> name == "")
			continue;
		if ($result["ratees"][$i] -> gender == null || $result["ratees"][$i] -> gender == "")
			continue;

		if ($result["ratees"][$i] -> email != null && $result["ratees"][$i] -> email != "")
			$result["ratees"][$i] -> orderRating += 3 * (1 + (mt_rand() / mt_getrandmax()));
		if ($result["ratees"][$i] -> profession != null && $result["ratees"][$i] -> profession != "")
			$result["ratees"][$i] -> orderRating += 1 * (1 + (mt_rand() / mt_getrandmax()));
		if ($result["ratees"][$i] -> photoLinkMedium != null && $result["ratees"][$i] -> photoLinkMedium != "")
			$result["ratees"][$i] -> orderRating += 3 * (1 + (mt_rand() / mt_getrandmax()));
		if ($result["ratees"][$i] -> birthday != null && $result["ratees"][$i] -> birthday != "")
			$result["ratees"][$i] -> orderRating += 2 * (1 + (mt_rand() / mt_getrandmax()));
		if ($result["ratees"][$i] -> isMember)
			$result["ratees"][$i] -> orderRating += 1 * (1 + (mt_rand() / mt_getrandmax()));
		if ($result["ratees"][$i] -> rateBefore)
			$result["ratees"][$i] -> orderRating += 1 * (1 + (mt_rand() / mt_getrandmax()));
	}

	usort($result["ratees"], 'rateeSort');
}

function rateeSort($a, $b) {
	$fa = (float)($a -> orderRating);
	$fb = (float)($b -> orderRating);

	if ($fa == $fb) {
		return 0;
	}
	return ($fa > $fb) ? -1 : 1;
}

function replaceByExistingContacts() {
	global $result, $fbIds, $gmail_emails, $mid, $me;

	$contactService = new ContactService();
	$pictureService = new PictureService();

	$contacts = $contactService -> loadByFbIdAndEmailArray($fbIds, $gmail_emails);

	$contactIds = array();
	// use to search existing rating records in the last two weeks.

	// for each ratee, check whether it has a contact record in database.
	// If yes use the information in the database
	// remove replicate ratee of the same contact
	for ($i = 0; $i < count($result["ratees"]); $i++) {

		foreach ($contacts as $contact) {
			//only facebook users or members could be rated. (The email of member is confirmed)
			if (($contact -> fbId != null && $contact -> fbId == $result["ratees"][$i] -> fbId) || ($contact -> userId != null && $contact -> email != null && $contact -> email == $result["ratees"][$i] -> email)) {

				if (in_array($contact -> id, $contactIds)) {
					unset($result["ratees"][$i]);
					break;
				}

				$result["ratees"][$i] -> contactId = $contact -> id;
				$result["ratees"][$i] -> rateBefore = true;

				$result["ratees"][$i] -> isMember = ($contact -> userId != null) ? true : false;
				$result["ratees"][$i] -> userId = $contact -> userId;

				$result["ratees"][$i] -> photoId = $contact -> mainPhotoId;
				if ($result["ratees"][$i] -> photoId != null) {
					$result["ratees"][$i] -> photoLinkMedium = $pictureService -> getMediumLink($result["ratees"][$i] -> photoId);
					$result["ratees"][$i] -> photoLinkSmall = $pictureService -> getSmallLink($result["ratees"][$i] -> photoId);
				}

				$result["ratees"][$i] -> name = $contact -> name;
				$result["ratees"][$i] -> email = $contact -> email;
				$result["ratees"][$i] -> fbId = $contact -> fbId;
				$result["ratees"][$i] -> profession = $contact -> profession;
				$result["ratees"][$i] -> birthday = $contact -> birthday;
				$result["ratees"][$i] -> majar = $contact -> majar;

				$result["ratees"][$i] -> birthday = $contact -> birthday;
				$result["ratees"][$i] -> gender = ($contact -> gender == 1) ? 1 : 2;

				array_push($contactIds, $contact -> id);

			}
		}
	}

	$result["ratees"] = array_values($result["ratees"]);

	$twoWeeksAgo = round(microtime(true) * 1000) - 14 * 24 * 60 * 60 * 1000;
	$existingRatings = $contactService -> getAllRatingsInTheLastPeriod($me -> id, $contactIds, $twoWeeksAgo);
	$size = count($result["ratees"]);
	for ($i = 0; $i < $size; $i++) {
		if ($result["ratees"][$i] -> contactId == $me -> contactId) {
			unset($result["ratees"][$i]);
			continue;
		}

		if ($result["ratees"][$i] -> gender == null || $result["ratees"][$i] -> gender == "") {
			unset($result["ratees"][$i]);
			continue;
		}

		//only ratee with fbIds are allowed to be rated
		if ($result["ratees"][$i] -> fbId == null || $result["ratees"][$i] -> fbId == "") {
			unset($result["ratees"][$i]);
			continue;
		}

		if ($result["ratees"][$i] -> contactId == null)
			continue;

		foreach ($existingRatings as $rating) {
			if ($rating -> targetContactId == $result["ratees"][$i] -> contactId) {
				unset($result["ratees"][$i]);
				continue;
			}
		}
	}

	$result["ratees"] = array_values($result["ratees"]);
}

function tryToGetFacebookRatees() {
	global $result, $fbIds;

	// try to get facebook contacts
	$facebook = new Facebook( array('appId' => FBConfig::$APP_ID, 'secret' => FBConfig::$SECRET, 'fileUpload' => true, 'cookie' => true, ));
	ini_set("allow_url_fopen", "On");
	$uid = $facebook -> getUser();

	// if facebook info is not available, set $result["facebook_available"] = false;
	if ($uid) {
		$myPermissions = $facebook -> api(array('query' => 'SELECT user_friends,friends_birthday FROM permissions where uid=' . $uid, 'method' => 'fql.query'));
		if ($myPermissions[0]["user_friends"] != 1 || $myPermissions[0]["user_friends"] != 1) {
			$result["facebook_available"] = false;
		} else {
			// if facebook is acessible
			getFacebookRatees($uid, $facebook);
			$result["facebook_available"] = true;
		}

	} else {
		$result["facebook_available"] = false;
	}
}

function getFacebookRatees($uid, $facebook) {
	global $result, $fbIds;

	$MultiQueryRes = $facebook -> api(array('queries' => array('FriendsDetailsQuery' => 'SELECT uid,name,sex,birthday_date,email,pic_big from user where uid in (select uid2 from friend where uid1=' . $uid . ')'), 'method' => 'fql.multiquery'));
	foreach ($MultiQueryRes as $QueryRes) {
		${$QueryRes['name']} = $QueryRes['fql_result_set'];
	}
	$FriendsDetails = $FriendsDetailsQuery;

	foreach ($FriendsDetails as $FriendDetails) {
		$ratee = new Ratee();

		$ratee -> source = "facebook";
		$ratee -> name = $FriendDetails["name"];
		$ratee -> fbId = $FriendDetails["uid"];

		$birthdayStrs = explode("/", $FriendDetails["birthday_date"]);
		$ratee -> birthday = (count($birthdayStrs) == 3) ? ($birthdayStrs[2] . "-" . $birthdayStrs[0] . "-" . $birthdayStrs[1]) : null;
		$ratee -> photoLinkMedium = $FriendDetails["pic_big"];
		$ratee -> photoLinkSmall = $FriendDetails["pic_big"];
		$ratee -> gender = ($FriendDetails["sex"] == "male" ? 1 : 2);

		$ratee -> rateBefore = false;
		$ratee -> isMember = false;

		array_push($result["ratees"], $ratee);
		array_push($fbIds, $ratee -> fbId);
	}
}

function tryToGetGmailRatees() {
	global $result, $gmail_emails;
	global $clientid, $clientsecret, $server_url, $API_key;

	$client = new Google_Client();
	$client -> setApplicationName('TestContacts');
	$client -> setScopes("http://www.google.com/m8/feeds/");

	$client -> setClientId($clientid);
	$client -> setClientSecret($clientsecret);
	$client -> setRedirectUri($server_url);
	$client -> setDeveloperKey($API_key);

	if (isset($_SESSION['token'])) {
		$client -> setAccessToken($_SESSION['token']);
	}

	if ($client -> getAccessToken()) {
		$req = new Google_HttpRequest("https://www.google.com/m8/feeds/contacts/default/full?v=3.0&max-results=9999");

		$val = $client -> getIo() -> authenticatedRequest($req);

		$content_xml = $val -> getResponseBody();
		getGmailRatees($content_xml);

		$result["gmail_available"] = true;

	} else {
		$result["gmail_available"] = false;
	}
}

function getGmailRatees($xml_response) {
	global $result, $gmail_emails;

	$xml = new SimpleXMLElement($xml_response);
	$xml -> registerXPathNamespace('gd', 'http://schemas.google.com/g/2005');

	foreach ($xml->entry as $entry) {
		$ratee = new Ratee();

		$ratee -> source = "facebook";

		$ratee = new Ratee();
		$ratee -> source = "gmail";

		foreach ($entry->xpath('gd:email') as $email) {
			// Get the email
			$p = (string)$email -> attributes() -> address;
			if ($p != NULL)
				$ratee -> email = (string)$p;
		}

		$ratee -> name = (string)$entry -> title;
		foreach ($entry->xpath('gd:name') as $nameFull) {
			$p = $nameFull -> children("gd", true) -> fullName;
			if ($p != NULL)
				$ratee -> name = (string)$p;
		}

		$ratee -> rateBefore = false;
		$ratee -> isMember = false;

		array_push($result["ratees"], $ratee);
		array_push($gmail_emails, $ratee -> email);

	}
}
?>