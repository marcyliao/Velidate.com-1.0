<?php
	//echo "Starting sendMessageReminders <br>";
	require_once __DIR__ . "/../service/MessageService.class.php";
	require_once __DIR__ . "/../service/EmailService.class.php";
	require_once __DIR__ . "/../dto/User.class.php";
	require_once __DIR__ . "/../dao/mysql/UserMySqlDAO.class.php";
	
	//echo "After requires <br>";
	$userDAO = new UserMySqlDAO();
	$result = $userDAO->queryWithRemindersLT3();
	$message = new MessageService();
	$emService = new EmailService(); 
	while ($row = mysql_fetch_array($result)) {
		$uid = $row['id'];
		//echo "Processing UID: $uid <br>";
		$nUnread = $message->getNumOfAllUnreadMessages($uid);
		if ($nUnread > 0) {
			$em = $row['email'];
			//echo "$em has unread messages: $nUnread <br>";
			error_log("$em has unread messages: $nUnread  " . date('Y-m-d H:i:s') . "\r\n", 3, "messageRemind.log");
			$emService->sendMessageReminder($em, $nUnread);
			$cuser = $userDAO->load($uid);
			$cuser->remindersSent = $cuser->remindersSent + 1;
			$userDAO->update($cuser);
		}
	}


?>