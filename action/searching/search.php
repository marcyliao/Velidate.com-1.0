<?php
require_once __DIR__ . '/../../dto/SearchCriteria.class.php';
require_once __DIR__ . '/../../service/SearchService.class.php';
require_once __DIR__ . "/../../service/AccountService.class.php";
require_once __DIR__ . "/../../utility/Utility.class.php";

session_start();
$id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;
if ($id == null) {
	Utility::message(406, "Your session has expired.");
}

$accountService = new AccountService();
$me = $accountService -> load($id);

$searchCriteria = new SearchCriteria();

$searchCriteria -> gender = isset($_GET["gender"]) ? $_GET["gender"] : $me -> seeking;
$searchCriteria -> from = isset($_GET["from"]) ? $_GET["from"] : null;
$searchCriteria -> to = isset($_GET["to"]) ? $_GET["to"] : null;
$searchCriteria -> city = isset($_GET["city"]) ? $_GET["city"] : null;
$searchCriteria -> country = isset($_GET["country"]) ? $_GET["country"] : null;
$searchCriteria -> meId = $id;
$page = isset($_GET["page"]) ? $_GET["page"] : 0;

$service = new SearchService();
echo $service -> search($searchCriteria, $page);
?>