<?php
	require_once __DIR__.'/../../dto/Photo.class.php';
	require_once __DIR__.'/../../service/PictureService.class.php';
	
 	$code = $_GET['code'];
	$photoService = new PictureService;
	
	$sizeid = $photoService->decrypt($code);
	$amp = strrpos($sizeid, '&');
	$pid = substr($sizeid, $amp + 1);
	$psize = substr($sizeid, 0, $amp);
	$pic = null;
	if ($psize == "o") {
		$pic = $photoService->getRealOriginalLink($pid);
	} else if ($psize == "m") {
		$pic = $photoService->getRealMediumLink($pid);
	} else if ($psize == "s") {
		$pic = $photoService->getRealSmallLink($pid);
	} else if ($psize == "ss") {
		$pic = $photoService->getRealBlurLink($pid);
	}
	if ($pic == null) {
		return null;
	} else {
		session_start(); 
		header("Cache-Control: private, max-age=10800, pre-check=10800");
		header("Pragma: private");
		header("Expires: " . date(DATE_RFC822,strtotime(" 2 day")));
		header('Content-Type: image/jpeg');
		if(isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])){
		  // if the browser has a cached version of this image, send 304
		  header('Last-Modified: '.$_SERVER['HTTP_IF_MODIFIED_SINCE'],true,304);
		  exit;
		}
		
		readfile($pic);
		exit;
	}

?>