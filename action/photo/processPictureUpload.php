<?php
    require_once __DIR__."/../../service/ContactService.class.php";
    require_once __DIR__."/../../service/AccountService.class.php";
    require_once __DIR__."/../../service/PictureService.class.php";
    require_once __DIR__."/../../utility/Utility.class.php";
    require_once __DIR__."/../../utility/Properties.class.php";
    require_once __DIR__."/../../dto/Photo.class.php";
    
    session_start();
    $id = $_SESSION['user_id'];
    if($id == null) {
   		Utility::redirect(Properties::$MESSAGE_PAGE."?msg=Please login first",false);
    }

    $accountService = new AccountService();
    $me = $accountService->load($id);

    if($me->accountStatus != User::CONFIRMED) {
    	$msg = "Hi, ".$me->userName
        	.". Your account is not activiated yet. We have sent a confirmation email to you."
        	." Please click the link inside to activate your account."
        	."<br>No email recieved? click <a href='".Properties::$RESEND_CONFIRMATION_ACTION."?id=".$id."'>here</a> to recieve the email again.";

    	Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$msg,false);

    }


    $contactService = new ContactService();

    $contact = $contactService->loadByUserId($me->id);

    $pictureService = new PictureService();
    $pictures = $pictureService->getUserPhotos($contact->id);
    $pic_count = count($pictures);

	$thispic = new Photo();
	
	try {
		
		$name = $_FILES["picture"]["name"];
		$ext = end(explode(".", $name));
		$filepath = Properties::$ROOT_PATH . Properties::$PHOTO_DIR. $contact->id  . "_tmp" . "." . $ext;

		move_uploaded_file ($_FILES['picture'] ['tmp_name'], $filepath);
		chmod($filepath, 0666);
		
		$thispic->contactId = $contact->id;
		$thispic->verified = 0;
		$thispic->notVerified = 0;
		$thispic->linkOriginal = "";
		$thispic->linkLarge = "";
		$thispic->linkMedium = "";
		$thispic->linkSmall = "";
		$thispic->visibility = 0;

		if($pic_count == 0) {
			$thispic->isMainPhoto = 1;
			
			$thispic->id = $pictureService->putpic($thispic, $filepath);
			
			$me->mainPhotoId = $thispic->id;
			$accountService->update($me);
			
			$contact->mainPhotoId = $thispic->id;
			$contactService->update($contact);
		} 
		else {
			$thispic->isMainPhoto = 0;
			
			$pictureService->putpic($thispic, $filepath);
		}
		
		// $thispic is an object of Photo Class.
		// $filepath contains the path and filename of the image
		// Convert photo to jpg, and store them in original, medium and small
		// Store in directory p0 for ID 1 to 999; p1 for ID 1000 to 1999 and so on.
		// File name is <ID>_o for original; _m for medium; _s for small.

        Utility::redirect(Properties::$PHOTOS_PAGE."?id=".$id."#last",false);

	}
    catch(Exception $e) {
        Utility::redirect(Properties::$MESSAGE_PAGE."?msg=".$e->getMessage(),false);
    }
		
		
	
?>
?>