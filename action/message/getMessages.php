<?php
require_once __DIR__ . "/../../service/MessageService.class.php";
require_once __DIR__ . "/../../utility/Utility.class.php";

session_start();
$id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;

if ($id == null) {
	Utility::message(406, "Your session has expired.");
}

$conversationId = isset($_GET['conversationId']) ? $_GET['conversationId'] : null;
if ($conversationId == null) {

	Utility::message(406, "Forbidden");
}

$messageService = new MessageService();
$conversation = $messageService -> loadConversation($conversationId);

if ($conversation -> userId1 != $id && $conversation -> userId2 != $id) {
	Utility::message(406, "Forbidden");
}

$messages = $messageService -> getMessageList($conversationId, round(microtime(true) * 1000), 50);

$directionFromMe = ($conversation -> userId1 == $id) ? 0 : 1;

foreach ($messages as $msg) {
	if ($msg -> direction == $directionFromMe)
		$msg -> fromMe = 1;
	else
		$msg -> fromMe = 0;
}

echo json_encode($messages);
?>