<?php
require_once __DIR__ . "/../../service/MessageService.class.php";
require_once __DIR__ . "/../../utility/Utility.class.php";

session_start();
$id = isset($_SESSION['user_id']) ? $_SESSION['user_id'] : null;

if ($id == null) {
	Utility::message(406, "Your session has expired.");
}

$messageId = isset($_GET['messageId']) ? $_GET['messageId'] : null;
if ($messageId == null) {

	Utility::message(406, "Forbidden");
}

$messageService = new MessageService();
$messageService -> readMessage($messageId);
?>