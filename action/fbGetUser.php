<?php
/**
 * Purpose of this file is to fetch Facebook profile info of the acting user and return it in JSON format.
 * This file can be accessed directly with valid URL. Once called it will first ask user to login to facebook.
 * If user is already logged in or Sings In after prompt app permission box will be displayed to get users permission to access profile information.
 * Prerequisite - correct appid, secret and canvasURL to be setup in config.php
 * Once setup any http call to the URL will follow the flow mentioned above and o/p JSON data with member Info
 * Output JSON in following format
 {
 "name":"",
 "email-address":"",
 "gender":"",
 "city":"",
 "province":"",
 "country":"",
 "religion":"",
 "date-of-birth":"",
 "profession":"",
 "highest-education":"",
 "height":""}
 */

require_once __DIR__ . "/../service/fbsrc/facebook.php";
require_once __DIR__ . "/../utility/Properties.class.php";
require_once __DIR__ . "/../utility/Utility.class.php";

$appId = "143547199131394";
//facebook app i
$secret = "f47cc20a2664fe81d7c353ee12e181f5";
//facebook app secret key

$facebook = new Facebook( array('appId' => $appId, 'secret' => $secret, 'fileUpload' => true, 'cookie' => true, ));
ini_set("allow_url_fopen", "On");

$thisPageName = Properties::$GET_FACEBOOK_USER_ACTION;
$uid = $facebook -> getUser();

if ($uid) {
	$myPermissions = $facebook -> api(array('query' => 'SELECT user_photos,user_friends,friends_birthday,email,user_location,user_religion_politics,user_birthday,user_work_history,user_education_history,friends_birthday FROM permissions where uid=' . $uid, 'method' => 'fql.query'));

	if ($myPermissions[0]["user_friends"] != 1 || $myPermissions[0]["user_friends"] != 1 || $myPermissions[0]["friends_birthday"] != 1 || $myPermissions[0]["email"] != 1 || $myPermissions[0]["user_location"] != 1 || $myPermissions[0]["user_religion_politics"] != 1 || $myPermissions[0]["user_birthday"] != 1 || $myPermissions[0]["user_work_history"] != 1 || $myPermissions[0]["user_education_history"] != 1 || $myPermissions[0]["friends_birthday"] != 1) {
		$par = array();
		$par['scope'] = "user_photos,friends_birthday,email,user_location,user_religion_politics,user_birthday,user_work_history,user_education_history,friends_birthday,user_photos";
		$par['canvas'] = 1;
		$par['fbconnect'] = 0;
		$par['redirect_uri'] = $thisPageName;
		$loginUrl = $facebook -> getLoginUrl($par);

		echo "<script language=javascript>window.open('$loginUrl', '_parent', '')</script>";
		exit();
	}
} else {
	$par = array();
	$par['scope'] = "email,user_location,user_religion_politics,user_birthday,user_work_history,user_education_history,friends_birthday,user_photos";
	$par['canvas'] = 1;
	$par['fbconnect'] = 0;
	$par['redirect_uri'] = $thisPageName;
	$loginUrl = $facebook -> getLoginUrl($par);

	echo "<script language=javascript>window.open('$loginUrl', '_parent', '')</script>";
	exit();
}

try {
	// Replace the following query (commented out) with tne query without PublicPicQuery.
	//$MultiQueryRes = $facebook -> api(array('queries' => array('UserDetailsQuery' => 'SELECT name,email,sex,current_location,religion,birthday_date,work,education,pic_big from user where uid=' . $uid, 'PublicPicsQuery' => 'SELECT src_big from photo where owner=' . $uid . ' LIMIT 5'), 'method' => 'fql.multiquery'));
	$MultiQueryRes = $facebook -> api(array('queries' => array('UserDetailsQuery' => 'SELECT name,email,sex,current_location,religion,birthday_date,work,education,pic_big from user where uid=' . $uid), 'method' => 'fql.multiquery'));

	foreach ($MultiQueryRes as $QueryRes) {
		${$QueryRes['name']} = $QueryRes['fql_result_set'];
	}
	// The following 4 lines were commented out as we are not getting public pics.
	$publicPics = array();
	//foreach ($PublicPicsQuery as $pic) {
	//	array_push($publicPics, $pic["src_big"]);
	//}

	$UserDetails = $UserDetailsQuery["0"];

	$UserName = $UserDetails["name"];
	$UserEMail = $UserDetails["email"];
	$UserGender = $UserDetails["sex"];
	$UserLocationArr = $UserDetails["current_location"];
	$UserReligion = $UserDetails["religion"];
	$UserDOB = $UserDetails["birthday_date"];
	$UserWorkHistory = $UserDetails["work"];
	$UserEducationHistory = $UserDetails["education"];
	//$UserPicture = $UserDetails["pic_big"];
	//array_unshift($publicPics, $UserPicture);
	array_push($publicPics, $UserDetails["pic_big"]);

	$UserHeight = "";
	$UserLocation = "";
	$UserProfession = "";
	$UserEducation = "";
	//echo "Gender: " . $UserGender . "<br>";
	if ($UserLocationArr != NULL && is_array($UserLocationArr) && array_key_exists("name", $UserLocationArr)) {
		$UserLocation = $UserLocationArr["name"];
	}

	/*
	 if ($UserWorkHistory != NULL && is_array($UserWorkHistory)) {
	 if ($UserWorkHistory["0"]["employer"] != NULL) {
	 if ($UserProfession == "")
	 $UserProfession = $UserWorkHistory["0"]["employer"]["name"];
	 else
	 $UserProfession = $UserProfession . "," . $UserWorkHistory["0"]["employer"]["name"];
	 }
	 if ($UserWorkHistory["0"]["position"] != NULL) {
	 if ($UserProfession == "")
	 $UserProfession = $UserWorkHistory["0"]["position"];
	 else
	 $UserProfession = $UserProfession . "," . $UserWorkHistory["0"]["position"];
	 }
	 }
	 *
	 */

	if ($UserEducationHistory != NULL && is_array($UserEducationHistory) && array_key_exists("0", $UserEducationHistory) && is_array($UserEducationHistory["0"]) && array_key_exists("type", $UserEducationHistory["0"])) {
		$UserEducation = $UserEducationHistory["0"]["type"];
	}

	$arr = array('name' => $UserName, 'fbid' => $uid, 'email-address' => $UserEMail, 'gender' => $UserGender, 'city' => $UserLocation, 'province' => $UserLocation, 'country' => $UserLocation, 'religion' => $UserReligion, 'date-of-birth' => $UserDOB, 'profession' => $UserProfession, 'highest-education' => $UserEducation, 'height' => $UserHeight);

	//header('Content-Type: application/json');
	//print_r (json_encode($arr));
	if (!session_id()) {
		session_start();
	}
	$_SESSION['userdata'] = $arr;
	$_SESSION['userpics'] = $publicPics;
	//var_dump($_SESSION['userdata']);
	$url = Properties::$SIGNUP_CONTINUE_PAGE;
	echo '<meta http-equiv="refresh" content="0; URL=' . $url . '" />';

} catch(FacebookApiException $e) {
	Utility::redirect(Properties::$MESSAGE_PAGE . "?msg=" . $e -> getMessage(), false);

}
?>