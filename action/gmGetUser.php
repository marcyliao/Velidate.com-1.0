<?php
	
	require_once __DIR__ . "/../service/gm/config.php";
	require_once __DIR__ . "/../service/gm/src/Google_Client.php";
    require_once __DIR__."/../utility/Properties.class.php";
    require_once __DIR__."/../utility/Utility.class.php";
	

	$client = new Google_Client();
	$client->setApplicationName('TestContacts');
	$client->setScopes("http://www.google.com/m8/feeds/");
	
	$client->setClientId($clientid);
	$client->setClientSecret($clientsecret);
	$client->setRedirectUri($server_url);
	$client->setDeveloperKey($API_key);
	
	session_start();
	if (isset($_GET['code'])) {
	  $client->authenticate();
	  $_SESSION['token'] = $client->getAccessToken();
      Utility::redirect(Properties::$PROFILE_PAGE,false);
	  exit();
	}
	
	if (isset($_SESSION['token'])) {
      Utility::redirect(Properties::$PROFILE_PAGE,false);
	  exit();
	}
	
	
    Utility::redirect("https://accounts.google.com/o/oauth2/auth?client_id=".$clientid."&redirect_uri=".($server_url)."&scope=https://www.google.com/m8/feeds/&response_type=code" ,false);
	
	
	
?>