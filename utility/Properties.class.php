<?php

class Properties{
	
	static public $DOMAIN;
	
	static public $ACTION_PATH;
	static public $VIEW_PATH;
	static public $SIGNUP_PAGE;
	
	static public $SIGNUP_CONTINUE_PAGE;
	static public $PROFILE_PAGE;
	static public $PROFILE_PAGE_RELATIVE;
	static public $PROFILE_VERIFICATION_TABLE_PAGE;
	static public $HOME_PAGE;
	static public $MESSAGE_PAGE;
	static public $MESSAGE_PAGE_RELATIVE;
	static public $PHOTOS_PAGE;
	static public $UPLOAD_PHOTO_PAGE;
	static public $SEARCH_PAGE;
	static public $EDIT_PROFILE_PAGE;
	static public $EDIT_PROFILE_PAGE_RELATIVE;
	static public $CONVERSATION_PAGE;
	static public $RATING_PAGE;
	static public $RATING_PAGE_RELATIVE;

	static public $LOGIN_ACTION;
	static public $LOGOUT_ACTION;
	static public $IS_EMAIL_UNIQUE_ACTION;
	static public $IS_STUDENT_EMAIL_VALID_ACTION;
	static public $COMPLETE_SIGNUP_ACTION;
	static public $CONFIRM_EMAIL_ACTION;
	static public $CONFIRM_STUDENT_EMAIL_ACTION;
	static public $PROCESS_PICTURE_UPLOAD_ACTION;
	static public $RESEND_CONFIRMATION_ACTION;
	static public $RESEND_STUDENT_CONFIRMATION_ACTION;
	static public $SEARCH_ACTION;
	static public $GET_FACEBOOK_USER_ACTION;
	static public $GET_GMAIL_USER_ACTION;
	static public $PROCESS_PROFILE_EDIT_ACTION;
	static public $PUSHER_AUTH_ACTION;
	static public $PHOTO_SET_VISIBILITY_ACTION;
	static public $PHOTO_SET_MAIN_PHOTO_ACTION;
	static public $PHOTO_DELETE_ACTION;
	static public $MESSAGE_GET_CONVERSATIONS_ACTION;
	static public $MESSAGE_GET_MESSAGES_ACTION;
	static public $MESSAGE_SEND_MESSAGE_ACTION;
	static public $MESSAGE_READ_CONVERSATION_ACTION;
	static public $MESSAGE_READ_MESSAGE_ACTION;
	static public $MESSAGE_GET_ALL_UNREAD_MESSAGE_COUNT_ACTION;
	static public $RATING_GET_RATEES_ACTION;
	static public $RATING_POST_RATING_ACTION;
	static public $PHOTO_GET_PHOTO_ACTION;
	
	static public $RDS_USERNAME;
	static public $RDS_PASSWORD;
	static public $RDS_HOST;
	static public $RDS_DATABASE;
	
	static public $PUSHER_KEY;
	static public $PUSHER_SECRET;
	static public $PUSHER_APP_ID;

	static public $PHOTO_PREX;
	static public $PHOTO_DIR;
	static public $SITE_DOMAIN;
	static public $ROOT_PATH;
	
	public static  function init(){
		self::$DOMAIN = "http://".$_SERVER['SERVER_NAME'];
		
		self::$ACTION_PATH = self::$DOMAIN."/action/";
		self::$VIEW_PATH = self::$DOMAIN."/view/html/";
		
		// view
		self::$SIGNUP_PAGE = self::$VIEW_PATH."signUp.php";
		self::$SIGNUP_CONTINUE_PAGE = self::$VIEW_PATH."signUpContinue.php";
		self::$PROFILE_VERIFICATION_TABLE_PAGE = self::$VIEW_PATH."profileVerificationTable.php";
		self::$PROFILE_PAGE = self::$VIEW_PATH."profile.php";
		self::$PROFILE_PAGE_RELATIVE = "/view/html/profile.php";
		self::$HOME_PAGE = self::$VIEW_PATH."home.php";
		self::$MESSAGE_PAGE = self::$VIEW_PATH."msg.php";
		self::$MESSAGE_PAGE_RELATIVE = "/view/html/msg.php";
		self::$PHOTOS_PAGE = self::$VIEW_PATH."photos.php";
		self::$UPLOAD_PHOTO_PAGE = self::$VIEW_PATH."uploadPic.php";
		self::$SEARCH_PAGE = self::$VIEW_PATH."search.php";
		self::$EDIT_PROFILE_PAGE = self::$VIEW_PATH."profile_edit.php";
		self::$EDIT_PROFILE_PAGE_RELATIVE = "/view/html/profile_edit.php";
		self::$CONVERSATION_PAGE = self::$VIEW_PATH."conversation.php";
		self::$RATING_PAGE = self::$VIEW_PATH."rate.php";
		self::$RATING_PAGE_RELATIVE = "/view/html/rate.php";
	
		//action
		//account
		self::$LOGIN_ACTION = self::$ACTION_PATH."account/login.php";
		self::$LOGOUT_ACTION = self::$ACTION_PATH."account/logout.php";
		self::$IS_EMAIL_UNIQUE_ACTION = self::$ACTION_PATH."account/isEmailUnique.php";
		self::$IS_STUDENT_EMAIL_VALID_ACTION = self::$ACTION_PATH."account/isStudentEmailValid.php";
		self::$COMPLETE_SIGNUP_ACTION = self::$ACTION_PATH."account/completeSignUp.php";
		self::$CONFIRM_EMAIL_ACTION = self::$ACTION_PATH."account/confirm.php";
		self::$CONFIRM_STUDENT_EMAIL_ACTION = self::$ACTION_PATH."account/studentConfirm.php";
		self::$RESEND_CONFIRMATION_ACTION = self::$ACTION_PATH."account/resendConfirmation.php";
		self::$RESEND_STUDENT_CONFIRMATION_ACTION = self::$ACTION_PATH."account/resendStudentConfirmation.php";
		self::$PROCESS_PROFILE_EDIT_ACTION = self::$ACTION_PATH."account/processProfileEdit.php";
		
		//search
		self::$SEARCH_ACTION = self::$ACTION_PATH."searching/search.php";
		
		//photo
		self::$PHOTO_SET_VISIBILITY_ACTION = self::$ACTION_PATH."photo/setVisibility.php";
		self::$PHOTO_SET_MAIN_PHOTO_ACTION = self::$ACTION_PATH."photo/setMainPhoto.php";
		self::$PHOTO_DELETE_ACTION = self::$ACTION_PATH."photo/delete.php";
		self::$PHOTO_GET_PHOTO_ACTION = self::$ACTION_PATH."photo/getPhoto.php";
		self::$PROCESS_PICTURE_UPLOAD_ACTION = self::$ACTION_PATH."photo/processPictureUpload.php";
		
		//message
		self::$PUSHER_AUTH_ACTION = self::$ACTION_PATH."message/auth.php";
		self::$MESSAGE_GET_CONVERSATIONS_ACTION = self::$ACTION_PATH."message/getConversations.php";
		self::$MESSAGE_GET_MESSAGES_ACTION = self::$ACTION_PATH."message/getMessages.php";
		self::$MESSAGE_SEND_MESSAGE_ACTION = self::$ACTION_PATH."message/sendMessage.php";
		self::$MESSAGE_READ_CONVERSATION_ACTION = self::$ACTION_PATH."message/readConversation.php";
		self::$MESSAGE_READ_MESSAGE_ACTION = self::$ACTION_PATH."message/readMessage.php";
		self::$MESSAGE_GET_ALL_UNREAD_MESSAGE_COUNT_ACTION = self::$ACTION_PATH."message/getUnreadMessagesCount.php";
		self::$RATING_GET_RATEES_ACTION = self::$ACTION_PATH."rating/getRatees.php";
		self::$RATING_POST_RATING_ACTION = self::$ACTION_PATH."rating/postRating.php";
		
		//facebook and gmail auth
		self::$GET_FACEBOOK_USER_ACTION = self::$ACTION_PATH."fbGetUser.php";
		self::$GET_GMAIL_USER_ACTION = self::$ACTION_PATH."gmGetUser.php";
		
		self::$ROOT_PATH = "/home/karandhanju/public_html/";
		self::$SITE_DOMAIN = "http://velidate.com/";
		
		
		//for production
		self::$PHOTO_DIR = "photos_pro/";
		self::$PHOTO_PREX = self::$SITE_DOMAIN.self::$PHOTO_DIR;
		self::$RDS_USERNAME = "support";
		self::$RDS_PASSWORD = "vElidate2#";
		self::$RDS_HOST = "localhost";
		self::$RDS_DATABASE = "velidate_production";
		self::$PUSHER_KEY = "36eca46a77cb13e91b97";
		self::$PUSHER_SECRET = "b1fb5f8e9dae267d75b7";
		self::$PUSHER_APP_ID = "87981";
		
		//for test
		// self::$PHOTO_DIR = "photos/";
		// self::$PHOTO_PREX = self::$SITE_DOMAIN.self::$PHOTO_DIR;
		// self::$RDS_USERNAME = "support";
		// self::$RDS_PASSWORD = "vElidate2#";
		// self::$RDS_HOST = "localhost";
		// self::$RDS_DATABASE = "db_velidate3";
		// self::$PUSHER_KEY = "36eca46a77cb13e91b97";
		// self::$PUSHER_SECRET = "b1fb5f8e9dae267d75b7";
	 	// self::$PUSHER_APP_ID = "87981";
	}	
}

Properties::init();

?>